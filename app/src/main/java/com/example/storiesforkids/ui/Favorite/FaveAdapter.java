package com.example.storiesforkids.ui.Favorite;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;
import com.bumptech.glide.Glide;
import com.example.storiesforkids.R;
import com.example.storiesforkids.ui.MainView.MainActivity;
import com.example.storiesforkids.ui.Simolor.ClickOnSimImg;

import java.util.List;

public class FaveAdapter extends RecyclerView.Adapter<FaveAdapter.ViewHolder> {

    private List<FaveData> dataList;
    private Context context;
   private ClickOnFaveItem clickOnFaveImg;


    public FaveAdapter(List<FaveData> dataList , Context context , MainActivity mainActivity) {

        this.dataList=dataList;
        this.context =context;
        this.clickOnFaveImg = mainActivity;

    }

    @NonNull
    @Override
    public FaveAdapter.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View v = LayoutInflater.from(parent.getContext()).inflate(R.layout.fave_item, parent, false);
        FaveAdapter.ViewHolder vh = new FaveAdapter.ViewHolder(v);
        return vh;
    }

    @Override
    public void onBindViewHolder(@NonNull ViewHolder holder, int position) {
        String img = dataList.get(position).getImageBookLink();
        String newString = img.replace("https", "http");

            Glide.with(context).load(newString).into(holder.img_book);

            holder.img_book.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {

                    clickOnFaveImg.goToDetailsClickedFave(dataList.get(position));

                }
            });


    }



    @Override
    public int getItemCount() {
        return dataList.size();
    }


    public class ViewHolder extends RecyclerView.ViewHolder {

       private ImageView img_book;



        public ViewHolder(View v) {
            super(v);

            img_book =  v.findViewById(R.id.img_fav_item);


        }
    }
}
