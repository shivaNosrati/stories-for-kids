package com.example.storiesforkids.ui.Favorite;

import com.example.storiesforkids.ui.Comments.Comment;
import com.example.storiesforkids.ui.MainFrg.CommentData;
import com.example.storiesforkids.ui.MainFrg.StoryParagraph;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.List;

public class Data {
    @SerializedName("id")
    @Expose
    private Integer id;

    @SerializedName("name")
    @Expose
    private String name;

    @SerializedName("title")
    @Expose
    private String title;

    @SerializedName("writer")
    @Expose
    private String writer;

    @SerializedName("publisher")
    @Expose
    private String publisher;

    @SerializedName("designer")
    @Expose
    private String designer;

    @SerializedName("talker")
    @Expose
    private String talker;

    @SerializedName("abstract")
    @Expose
    private String _abstract;

    @SerializedName("age")
    @Expose
    private String age;

    @SerializedName("view_count")
    @Expose
    private Integer viewCount;

    @SerializedName("download_count")
    @Expose
    private Integer downloadCount;

    @SerializedName("section_body")
    @Expose
    private String sectionBody;

    @SerializedName("comment")
    @Expose
    private List<CommentData> comment ;

    @SerializedName("is_like")
    @Expose
    private Boolean is_like ;

    @SerializedName("voice_link")
    @Expose
    private String voiceLink;

    @SerializedName("voice_size")
    @Expose
    private Integer voiceSize;

    @SerializedName("story_paragraph")
    @Expose
    private List<StoryParagraph> storyParagraph ;

    @SerializedName("image_book_link")
    @Expose
    private String imageBookLink;

    @SerializedName("favorite_count")
    @Expose
    private Integer favorite_count;

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String get_abstract() {
        return _abstract;
    }

    public Boolean getIs_like() {
        return is_like;
    }

    public void set_abstract(String _abstract) {
        this._abstract = _abstract;
    }

    public void setIs_like(Boolean is_like) {
        this.is_like = is_like;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getWriter() {
        return writer;
    }

    public void setWriter(String writer) {
        this.writer = writer;
    }

    public String getPublisher() {
        return publisher;
    }

    public void setPublisher(String publisher) {
        this.publisher = publisher;
    }

    public String getDesigner() {
        return designer;
    }

    public void setDesigner(String designer) {
        this.designer = designer;
    }

    public String getTalker() {
        return talker;
    }

    public void setTalker(String talker) {
        this.talker = talker;
    }

    public String getAbstract() {
        return _abstract;
    }

    public void setAbstract(String _abstract) {
        this._abstract = _abstract;
    }

    public String getAge() {
        return age;
    }

    public void setAge(String age) {
        this.age = age;
    }

    public Integer getViewCount() {
        return viewCount;
    }

    public void setViewCount(Integer viewCount) {
        this.viewCount = viewCount;
    }

    public Integer getDownloadCount() {
        return downloadCount;
    }

    public void setDownloadCount(Integer downloadCount) {
        this.downloadCount = downloadCount;
    }


    public String getSectionBody() {
        return sectionBody;
    }

    public void setSectionBody(String sectionBody) {
        this.sectionBody = sectionBody;
    }

    public List<CommentData> getComment() {
        return comment;
    }

    public void setComment(List<CommentData> comment) {
        this.comment = comment;
    }

    public String getVoiceLink() {
        return voiceLink;
    }

    public void setVoiceLink(String voiceLink) {
        this.voiceLink = voiceLink;
    }

    public Integer getVoiceSize() {
        return voiceSize;
    }

    public void setVoiceSize(Integer voiceSize) {
        this.voiceSize = voiceSize;
    }

    public List<StoryParagraph> getStoryParagraph() {
        return storyParagraph;
    }

    public void setStoryParagraph(List<StoryParagraph> storyParagraph) {
        this.storyParagraph = storyParagraph;
    }

    public String getImageBookLink() {
        return imageBookLink;
    }

    public void setImageBookLink(String imageBookLink) {
        this.imageBookLink = imageBookLink;
    }

    public void setFavorite_count(Integer favorite_count) {
        this.favorite_count = favorite_count;
    }

    public Integer getFavorite_count() {
        return favorite_count;
    }
}
