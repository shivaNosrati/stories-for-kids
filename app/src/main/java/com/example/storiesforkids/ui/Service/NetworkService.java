package com.example.storiesforkids.ui.Service;

import android.content.Context;
import android.util.Log;

import com.example.storiesforkids.ui.AllOfStoryInfo.DisLikeCallback;
import com.example.storiesforkids.ui.AllOfStoryInfo.SetFaveCallback;
import com.example.storiesforkids.ui.Comments.CommentModel;
import com.example.storiesforkids.ui.Comments.GetCommentsCallback;
import com.example.storiesforkids.ui.Favorite.FavoritModel;
import com.example.storiesforkids.ui.Favorite.GetFaveCallBack;
import com.example.storiesforkids.ui.Game.GameModel;
import com.example.storiesforkids.ui.History.GetHistoryCallback;
import com.example.storiesforkids.ui.History.HistoryModel;
import com.example.storiesforkids.ui.History.SetHistoryCallback;
import com.example.storiesforkids.ui.LoginRegister.CodeRegisterModel;
import com.example.storiesforkids.ui.LoginRegister.LogOutCallBack;
import com.example.storiesforkids.ui.LoginRegister.PhoneRegisterModel;
import com.example.storiesforkids.ui.LoginRegister.RegisterCallBack;
import com.example.storiesforkids.ui.LoginRegister.VerifyCallBack;
import com.example.storiesforkids.ui.MainFrg.GetStoriesCallback;
import com.example.storiesforkids.ui.MainFrg.InfoStoryModel;
import com.example.storiesforkids.ui.APIClient.RetrofitInstance;
import com.example.storiesforkids.ui.Search.InfoSearchModel;
import com.example.storiesforkids.ui.Search.ResponsesSearch;
import com.example.storiesforkids.ui.Simolor.GetSimilorsCallback;
import com.example.storiesforkids.ui.StoryText.GetGamesCallback;
import com.example.storiesforkids.ui.PlayVoice.ParagraphModel;
import com.example.storiesforkids.ui.PlayVoice.Paragraphcallback;
import com.example.storiesforkids.ui.UserInformation.UserInfoCallBack;
import com.example.storiesforkids.ui.UserInformation.UserInfoModel;
import com.example.storiesforkids.ui.UserInformation.UserInfoWithoutPhone;
import com.example.storiesforkids.ui.UserInformation.UserInformationModel;
import okhttp3.MediaType;
import okhttp3.MultipartBody;
import okhttp3.RequestBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class NetworkService implements INetworkServices {

    private static NetworkService ns;

    public static synchronized NetworkService getService() {
        if (ns != null)
            return ns;
        ns = new NetworkService();
        return ns;
    }


    @Override
    public void registerWithPhone(String phoneNumber, String uuId, RegisterCallBack callBack ) {
        APIService service =RetrofitInstance.getRetrofitInstance().create(APIService.class);
        Call<PhoneRegisterModel> call = service.userRegisterWithNum(phoneNumber, uuId);
        call.enqueue(new Callback<PhoneRegisterModel>() {
            @Override
            public void onResponse(Call<PhoneRegisterModel> call, Response<PhoneRegisterModel> response) {
                if (response.body()!= null && response.body().getCode() != -1)
                    callBack.doRegister();
                else if (response.body()!= null && response.body().getCode() == -1)
                    callBack.dublicateNumber();
            }

            @Override
            public void onFailure(Call<PhoneRegisterModel> call, Throwable t) {

                callBack.notRegister(t);
            }
        });
    }

    @Override
    public void registerWithCode(String phoneNumber, String uuId, Integer code, VerifyCallBack callBack) {
        APIService service =RetrofitInstance.getRetrofitInstance().create(APIService.class);
        Call<CodeRegisterModel> call = service.userRegisterWithCode(phoneNumber,uuId ,code);
        call.enqueue(new Callback<CodeRegisterModel>() {
            @Override
            public void onResponse(Call<CodeRegisterModel> call, Response<CodeRegisterModel> response) {

                if (response.body() != null && response.body().getData() != null &&response.body().getCode()==1 ) {

                    callBack.onReceiveToken(response.body().getData());

                }else
                    if (response.body() != null && response.body().getCode() == -1 )
                    callBack.onDublicate();

        }

            @Override
            public void onFailure(Call<CodeRegisterModel> call, Throwable t) {

                callBack.notReceive();
            }
        });
    }

    @Override
    public void getStories(String token, GetStoriesCallback callback ) {
       APIService service = RetrofitInstance.getRetrofitInstance().create(APIService.class);
       // APIService service = RetrofitInstance.getRetrofitInstance(true).create(APIService.class);
        Call<InfoStoryModel> call = service.getDetails(token);
        call.enqueue(new Callback<InfoStoryModel>() {
            @Override
            public void onResponse(Call<InfoStoryModel> call, Response<InfoStoryModel> response) {

                if (response.raw().cacheResponse() != null) {
                    callback.onReceived(response.body());
                    Log.e("Network", "response came from cache");
                }

                if (response.raw().networkResponse() != null) {
                    Log.e("Network", "response came from server");
                    callback.onReceived(response.body());
                }

            }

            @Override
            public void onFailure(Call<InfoStoryModel> call, Throwable t) {

                callback.notReceived(t);
                Log.e("error in get Story" , ""+t.getMessage());
            }
        });
    }


    @Override
    public void setFav(Integer id, String token, SetFaveCallback callback) {
        APIService service = RetrofitInstance.getRetrofitInstance().create(APIService.class);
        Call<PhoneRegisterModel> call = service.setFav(id , token );
        call.enqueue(new Callback<PhoneRegisterModel>() {
            @Override
            public void onResponse(Call<PhoneRegisterModel> call, Response<PhoneRegisterModel> response) {

                if (response.body()!= null)
                   callback.storyIsFave();

                if (response.isSuccessful())
                    Log.i("ee", "onResponse: ");
            }

            @Override
            public void onFailure(Call<PhoneRegisterModel> call, Throwable t) {

                callback.storyNotSetToFave(t);
            }
        });
    }

    @Override
    public void disLike(Integer id, String token, DisLikeCallback callback) {
        APIService service = RetrofitInstance.getRetrofitInstance().create(APIService.class);
        Call<PhoneRegisterModel> call = service.setDisLike(id , token);
        call.enqueue(new Callback<PhoneRegisterModel>() {
            @Override
            public void onResponse(Call<PhoneRegisterModel> call, Response<PhoneRegisterModel> response) {
                if (response.body() != null && response.body().getCode()==1){
                    callback.storyIsDisLiked();
                }
                assert response.body() != null;
                if (response.body().getCode()== -1)
                    callback.Error();
                Log.e("dislike eroor" ,""+response.body().getMessage()+id);
            }

            @Override
            public void onFailure(Call<PhoneRegisterModel> call, Throwable t) {

                callback.storyIsNotDisLiked(t);
            }
        });
    }

    @Override
    public void getFav(String token, GetFaveCallBack callback) {
        APIService service = RetrofitInstance.getRetrofitInstance().create(APIService.class);
        Call<FavoritModel> call = service.getFaoriteStory(token);
        call.enqueue(new Callback<FavoritModel>() {
            @Override
            public void onResponse(Call<FavoritModel> call, Response<FavoritModel> response) {
                if (response.body()!= null){
                    callback.faveListReceived(response.body());
                    Log.i("asd" , ""+response.body().getData().size());

                }
            }

            @Override
            public void onFailure(Call<FavoritModel> call, Throwable t) {

                callback.faveListNotReceived(t);
            }
        });
    }

    @Override
    public void getSim(Integer id, String token, GetSimilorsCallback callback) {
        APIService service =RetrofitInstance.getRetrofitInstance().create(APIService.class);
        Call<InfoStoryModel> call = service.getSimilorStories(id , token);
        call.enqueue(new Callback<InfoStoryModel>() {
            @Override
            public void onResponse(Call<InfoStoryModel> call, Response<InfoStoryModel> response) {
                if (response.body()!= null)
                    callback.onReceived(response.body());

            }

            @Override
            public void onFailure(Call<InfoStoryModel> call, Throwable t) {


                callback.notReceived(t);
            }
        });
    }

    @Override
    public void setHis(Integer id, String token, SetHistoryCallback callback) {

        APIService service = RetrofitInstance.getRetrofitInstance().create(APIService.class);
        Call<PhoneRegisterModel> call = service.setHistory(id,token);
        call.enqueue(new Callback<PhoneRegisterModel>() {
            @Override
            public void onResponse(Call<PhoneRegisterModel> call, Response<PhoneRegisterModel> response) {

                if (response.body()!= null )
                    callback.historyIsSet();

                if (response.isSuccessful())
                    Log.i("ee", "onResponse: ");
            }

            @Override
            public void onFailure(Call<PhoneRegisterModel> call, Throwable t) {

                callback.historyNotSet(t);
            }
        });

    }

    @Override
    public void getHis(String token, GetHistoryCallback callback) {
        APIService service = RetrofitInstance.getRetrofitInstance().create(APIService.class);
        Call<HistoryModel> call = service.getHistory(token);
        call.enqueue(new Callback<HistoryModel>() {
            @Override
            public void onResponse(Call<HistoryModel> call, Response<HistoryModel> response) {
                if (response.body() != null && response.body().getData() != null)
                    callback.historyIsSet(response.body());
            }

            @Override
            public void onFailure(Call<HistoryModel> call, Throwable t) {

                callback.historyNotSet(t);
            }
        });
    }

    @Override
    public void getsearchresponse(String strSearch,String token, ResponsesSearch callBack) {
        APIService service = RetrofitInstance.getRetrofitInstance().create(APIService.class);
        Call<InfoSearchModel> call = service.getResponseSearch(strSearch,token);
        call.enqueue(new Callback<InfoSearchModel>() {
            @Override
            public void onResponse(Call<InfoSearchModel> call, Response<InfoSearchModel> response) {
                if (response.body() != null && response.body().getData() != null)
                    callBack.onResponse(response.body());
            }

            @Override
            public void onFailure(Call<InfoSearchModel> call, Throwable t) {
                callBack.onFailure(t);
            }
        });
    }

    @Override
    public void setProfile(String token, UserInformationModel model, RegisterCallBack callback) {
        APIService service  = RetrofitInstance.getRetrofitInstance().create(APIService.class);
//        RequestBody pic1 = RequestBody.create(MediaType.parse("image/*"), model.getPic());
//        MultipartBody.Part pic = MultipartBody.Part.createFormData("pic", model.getPic().getName(), pic1);

        RequestBody name = RequestBody.create(MediaType.parse("text/plain"), model.getName());
        RequestBody age = RequestBody.create(MediaType.parse("text/plain"), model.getAge().toString());
        RequestBody uu_id = RequestBody.create(MediaType.parse("text/plain"), model.getUu_id());


        Call<PhoneRegisterModel> call = service.setuser(token , name , age , uu_id );
        call.enqueue(new Callback<PhoneRegisterModel>() {
            @Override
            public void onResponse(Call<PhoneRegisterModel> call, Response<PhoneRegisterModel> response) {
                if (response.body() != null)
                    callback.doRegister();
                Log.e("yuyu" ,""+ response.body());
                if (response.body().getCode() == -1) callback.dublicateNumber();


            }

            @Override
            public void onFailure(Call<PhoneRegisterModel> call, Throwable t) {
                callback.notRegister(t);
            }
        });
    }


    @Override
    public void logOut(String token, LogOutCallBack callBack) {
        APIService service = RetrofitInstance.getRetrofitInstance().create(APIService.class);
        Call<PhoneRegisterModel> call = service.logOut(token);
        call.enqueue(new Callback<PhoneRegisterModel>() {
            @Override
            public void onResponse(Call<PhoneRegisterModel> call, Response<PhoneRegisterModel> response) {
                if (response.body() != null && response.body().getCode()!= -1)
                    callBack.userIsLogout();
            }

            @Override
            public void onFailure(Call<PhoneRegisterModel> call, Throwable t) {

                callBack.userNotLogout(t);
            }
        });

    }

    @Override
    public void userInformation(String token, UserInfoCallBack callback) {
        APIService service = RetrofitInstance.getRetrofitInstance().create(APIService.class);
        Call<UserInfoModel> call = service.userInfo(token);
        call.enqueue(new Callback<UserInfoModel>() {
            @Override
            public void onResponse(Call<UserInfoModel> call, Response<UserInfoModel> response) {
                if (response.body() != null && response.isSuccessful() && response.body().getData()!= null)
                    callback.userInfoIsAvailable(response.body());
            }

            @Override
            public void onFailure(Call<UserInfoModel> call, Throwable t) {
                callback.infoNotAvailable(t);

            }
        });

    }

    @Override
    public void updateUser(String token, UserInfoWithoutPhone model, RegisterCallBack callback) {
        APIService service = RetrofitInstance.getRetrofitInstance().create(APIService.class);
        RequestBody pic1;
        MultipartBody.Part pic;
        if (model.getPic() == null){

//            pic1 =null;
//            pic = MultipartBody.Part.createFormData("pic",null, pic1);

            RequestBody attachmentEmpty = RequestBody.create(MediaType.parse("image/*"), "");

            pic = MultipartBody.Part.createFormData("pic", "", attachmentEmpty);

        }else {

            pic1 = RequestBody.create(MediaType.parse("image/*"), model.getPic());

            pic = MultipartBody.Part.createFormData("pic", model.getPic().getName(), pic1);

        }

//        if (model.getPhone()== null){
//
//            RequestBody phone = RequestBody.create(MediaType.parse("text/plain"), model.getPhone());
//
//
//        }else {
//
//            RequestBody phone = RequestBody.create(MediaType.parse("text/plain"), model.getPhone());
//
//        }


        RequestBody name = RequestBody.create(MediaType.parse("text/plain"), model.getName());
        RequestBody age = RequestBody.create(MediaType.parse("text/plain"), model.getAge().toString());
        RequestBody uu_id = RequestBody.create(MediaType.parse("text/plain"), model.getUu_id());
        RequestBody phone = RequestBody.create(MediaType.parse("text/plain"), model.getPhone());


        Call<PhoneRegisterModel> call = service.updateUser(token,name,age,uu_id,pic ,phone);
        call.enqueue(new Callback<PhoneRegisterModel>() {
            @Override
            public void onResponse(Call<PhoneRegisterModel> call, Response<PhoneRegisterModel> response) {
                if (response.body() != null && response.body().getCode() ==1)
                    callback.doRegister();

                if (response.body() != null && response.body().getCode() == -1)
                    callback.dublicateNumber();
            }

            @Override
            public void onFailure(Call<PhoneRegisterModel> call, Throwable t) {

                callback.notRegister(t);
            }
        });
    }


    @Override
    public void updateVerif(String token, String phone, String uuid, Integer code, VerifyCallBack callback) {
        APIService service = RetrofitInstance.getRetrofitInstance().create(APIService.class);
        Call<CodeRegisterModel> call = service.updateVerification(token ,phone ,uuid ,code);
        call.enqueue(new Callback<CodeRegisterModel>() {
            @Override
            public void onResponse(Call<CodeRegisterModel> call, Response<CodeRegisterModel> response) {
                if (response.body()!= null && response.body().getCode() ==1 )

                    callback.onReceiveToken(response.body().getData());

                if (response.body() != null && response.body().getCode()==-1)
                    callback.onDublicate();
            }

            @Override
            public void onFailure(Call<CodeRegisterModel> call, Throwable t) {

                callback.notReceive();
            }
        });
    }


    @Override
    public void userComment(Integer id, String token, GetCommentsCallback callback) {
        APIService service = RetrofitInstance.getRetrofitInstance().create(APIService.class);
        Call<CommentModel> call = service.getComments(id , token);
        call.enqueue(new Callback<CommentModel>() {
            @Override
            public void onResponse(Call<CommentModel> call, Response<CommentModel> response) {


                if (response.body()!= null && response.body().getData() !=null){
                    callback.onReceive(response.body());
                }
            }

            @Override
            public void onFailure(Call<CommentModel> call, Throwable t) {

                callback.notReceive(t);
            }
        });
    }

    @Override
    public void setComment(String body, Integer id, String token, SetFaveCallback callback) {
        APIService service = RetrofitInstance.getRetrofitInstance().create(APIService.class);
        Call<PhoneRegisterModel> call = service.setComment(id , body , token);
        call.enqueue(new Callback<PhoneRegisterModel>() {
            @Override
            public void onResponse(Call<PhoneRegisterModel> call, Response<PhoneRegisterModel> response) {
                if (response.body()!= null && response.body().getCode() ==1)
                    callback.storyIsFave();
            }

            @Override
            public void onFailure(Call<PhoneRegisterModel> call, Throwable t) {

                callback.storyNotSetToFave(t);
            }
        });
    }

    @Override
    public void getGames(String token, GetGamesCallback callback) {

        APIService service = RetrofitInstance.getRetrofitInstance().create(APIService.class);
        Call<GameModel> call = service.getGames(token);
        call.enqueue(new Callback<GameModel>() {
            @Override
            public void onResponse(Call<GameModel> call, Response<GameModel> response) {
                if (response.body()!= null && response.body().getData()!=null)
                    callback.gameReceived(response.body());
            }

            @Override
            public void onFailure(Call<GameModel> call, Throwable t) {

                callback.gameNotReceive(t);
            }
        });
    }

    @Override
    public void getParagraph(String token, Integer id, Paragraphcallback callback) {

        APIService service = RetrofitInstance.getRetrofitInstance().create(APIService.class);
        Call<ParagraphModel> call = service.getParagraph(id , token);
        call.enqueue(new Callback<ParagraphModel>() {
            @Override
            public void onResponse(Call<ParagraphModel> call, Response<ParagraphModel> response) {
                if (response.body() != null && response.body().getData() != null)
                    callback.paragraghReseived(response.body());
            }

            @Override
            public void onFailure(Call<ParagraphModel> call, Throwable t) {

                callback.paragraghNotReseived(t);
            }
        });
    }


}


