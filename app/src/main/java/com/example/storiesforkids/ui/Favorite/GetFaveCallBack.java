package com.example.storiesforkids.ui.Favorite;

import com.example.storiesforkids.ui.MainFrg.InfoStoryModel;

public interface GetFaveCallBack {

    void faveListReceived(FavoritModel favoritModel);
    void faveListNotReceived(Throwable t);
}
