package com.example.storiesforkids.ui.UserInformation;

import android.Manifest;
import android.annotation.TargetApi;
import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.graphics.Bitmap;
import android.media.AudioManager;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.provider.MediaStore;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.SeekBar;
import android.widget.Toast;
import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.ActionMenuView;
import androidx.appcompat.widget.Toolbar;
import androidx.core.app.ActivityCompat;
import androidx.core.content.ContextCompat;
import androidx.lifecycle.LiveData;
import androidx.lifecycle.MutableLiveData;
import androidx.lifecycle.Observer;
import androidx.lifecycle.Transformations;
import androidx.lifecycle.ViewModelProviders;
import androidx.navigation.NavController;
import androidx.navigation.Navigation;
import com.bumptech.glide.Glide;
import com.bumptech.glide.load.engine.DiskCacheStrategy;
import com.bumptech.glide.request.RequestOptions;
import com.example.storiesforkids.R;
import com.example.storiesforkids.ui.BaseFragment;
import com.example.storiesforkids.ui.LoginRegister.LogOutCallBack;
import com.example.storiesforkids.ui.LoginRegister.RegisterCallBack;
import com.example.storiesforkids.ui.MainView.MainActivity;
import com.example.storiesforkids.ui.MainView.SomeChangeInView;
import com.example.storiesforkids.ui.Service.NetworkService;
import com.example.storiesforkids.ui.Share.SharePreferencesHelper;
import com.example.storiesforkids.ui.mvvm.SViewModel;
import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.Objects;
import butterknife.BindView;
import butterknife.ButterKnife;
import de.hdodenhof.circleimageview.CircleImageView;

import static com.example.storiesforkids.ui.mvvm.SViewModel.userleLiveData;

public class FragmentProfile extends BaseFragment implements IbackPressListener{

    @BindView(R.id.p_toolbar)
    Toolbar p_toolbar;

    @BindView(R.id.editText_name)
    EditText sName;

    @BindView(R.id.editText_phone)
    EditText phone;

    @BindView(R.id.button_edit)
    Button edit;

    @BindView(R.id.pBack)
    ImageView btn_back;

    @BindView(R.id.change_img)
    CircleImageView img_user;

    @BindView(R.id.amvMenu)
    ActionMenuView menuView;

    @BindView(R.id.seekBar_voice)
    SeekBar volumeSeekbar;

    @BindView(R.id.seekBar_font)
    SeekBar fontSeekbar;

    private ProgressDialog progressDialog;
    private boolean result;
    private UserInfoWithoutPhone model;
    private Integer age;
    private String phoneNumber ;
    private String token;
    private String uuid;
    private int checkUp;
    private boolean isvalidPic = false;
    private boolean isValidName = true;
    private boolean isValidPhone =true;
    private NavController navController;

    GetGhange getGhange;
    SomeChangeInView changeUiListener;
    private SViewModel viewModel;
    private static MutableLiveData<UserInfo> infoLiveDat  = new MutableLiveData<>();

    private static LiveData<UserInfo> metaData = new MutableLiveData<>();

    @Override
    public void backPress() {
        back();
    }

    public interface GetGhange{
        void isGhanged();
    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {

        View view = inflater.inflate(R.layout.edit_profile_frg, container, false);

        navController = Navigation.findNavController(getActivity(), R.id.nav_host_fragment);

        ((MainActivity) getActivity()).updateStatusBarColor("#CD0202");

        ButterKnife.bind(this, view);
        setHasOptionsMenu(true);
        ((AppCompatActivity) getActivity()).setSupportActionBar(p_toolbar);
        p_toolbar.setTitle("");




        return view;
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {

        super.onViewCreated(view, savedInstanceState);

    }


    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        token = SharePreferencesHelper.getInatance().getStringValueFromShare(getContext());
        uuid = SharePreferencesHelper.getInatance().getUuidStringValueFromShare(getContext());
        changeUiListener.onFragmentChangeListener(this);

//        getUserInfo(token);
        getProfileInfo();
        getImageForEdit();
        setMenu();
        back();
        initVoiceControllers();
        initFontController();
        clickForEdit();
    }

    private void getProfileInfo() {

        if (userleLiveData.getValue() == null){
            viewModel = ViewModelProviders.of(Objects.requireNonNull(getActivity())).get(SViewModel.class);
            viewModel.getInfo().observe(this, new Observer<UserInfo>() {
                @Override
                public void onChanged(@Nullable UserInfo blogList) {


                    infoLiveDat.setValue(blogList);

                  // Log.e("tagList" , "finalList sze"+finalLiveDat.getValue().getName());
                    Log.e("ere" ,"uuuuuuuuuuuuu"+blogList.getName() );
                    fillProfile(infoLiveDat);

                }
            });
        }
        else {
            MutableLiveData<UserInfo> lastData = new MutableLiveData<>();
            lastData.setValue(userleLiveData.getValue());
            fillProfile(lastData);
            Log.e("data" ,"data not null" );
        }
        }



    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        switch (requestCode) {
            case 0: {
                // If request is cancelled, the result arrays are empty.
                if (grantResults.length > 0 && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                    Intent takePicture = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
                    startActivityForResult(takePicture, 0);
                } else {
                    Toast.makeText(getContext(), "Approve permissions to open Pix ImagePicker", Toast.LENGTH_LONG).show();
                }
                return;
            }
        }
    }

    @Override
    public void onAttach(@NonNull Context context) {
        super.onAttach(context);
        if (context instanceof GetGhange){
            this.getGhange = (GetGhange) context;
        }
        if(context instanceof SomeChangeInView)
            this.changeUiListener= (SomeChangeInView) context;


    }

    @Override
    public void onDetach() {
        super.onDetach();
        getGhange= null;
    }

    @Override
    public void onResume() {
//        progressDialog.dismiss();
        super.onResume();
        if(getContext() instanceof SomeChangeInView)
            this.changeUiListener= (SomeChangeInView) getContext();
        changeUiListener.onFragmentChangeListener(this);

    }

    private void clickForEdit() {
        model = new UserInfoWithoutPhone();
        edit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Register();
            }
        });

    }

    private void Register() {

        if (!validate()) {
            return;
        }
//        edit.setClickable(false);


        if (sName.getText() != null && phone.getText()!=null) {

            model.setAge(age);
            model.setUu_id(uuid);
            model.setName(sName.getText().toString());
            model.setPhone(phone.getText().toString());

            if (!isvalidPic){
                model.setPic(null);
            }

            if (phoneNumber.equals(phone.getText().toString())){

                model.setPhone("");
                sendRequestToUpdate(token , model);
                edit.setClickable(false);
                progressDialog = new ProgressDialog(getContext(),R.style.AppTheme_Green_Dialog);
                progressDialog.setIndeterminate(true);
                progressDialog.setMessage("شکیبا باشید... ");
                progressDialog.show();
                progressDialog.setCancelable(true);
            }
            else {

                if (isValidName && isValidPhone && phoneNumber != phone.getText().toString())

                    model.setPhone(phone.getText().toString());
                sendRequestToSecondUpdate(token , model );

                progressDialog = new ProgressDialog(getContext(),R.style.AppTheme_Green_Dialog);
                progressDialog.setIndeterminate(true);
                progressDialog.setMessage("شکیبا باشید... ");
                progressDialog.show();
                progressDialog.setCancelable(true);
                edit.setClickable(false);

            }

        }
    }

    private boolean validate() {
        boolean valid = true;

        String n = sName.getText().toString();
        String p = phone.getText().toString();

        if (n.isEmpty()) {
            sName.setError("نام وارد نمایید");
            edit.setClickable(true);
            valid = false;
        }else
            if (p.isEmpty() || p.length() < 11){
                phone.setError("شماره تلفن  معتبری وارد نمایید");
                edit.setClickable(true);
                valid = false;
            }
         else

           sName.setError(null);
        phone.setError(null);

        return valid;
    }

    private void initFontController() {

        fontSeekbar.setOnSeekBarChangeListener(new SeekBar.OnSeekBarChangeListener() {

            @Override
            public void onStopTrackingTouch(SeekBar seekBar) {
                // TODO Auto-generated method stub
            }

            @Override
            public void onStartTrackingTouch(SeekBar seekBar) {
                // TODO Auto-generated method stub
            }

            @Override
            public void onProgressChanged(SeekBar seekBar, int progress,boolean fromUser) {
                // TODO Auto-generated method stub

//                Text.setTextSize(progress);
//                StoryContentFragment.contextS.setTextScaleX(progress);

            }
        });
    }

    private void initVoiceControllers(){
        try

        {
            AudioManager audioManager = (AudioManager) getActivity().getSystemService(getContext().AUDIO_SERVICE);
            volumeSeekbar.setMax(audioManager
                    .getStreamMaxVolume(AudioManager.STREAM_MUSIC));
            volumeSeekbar.setProgress(audioManager
                    .getStreamVolume(AudioManager.STREAM_MUSIC));


            volumeSeekbar.setOnSeekBarChangeListener(new SeekBar.OnSeekBarChangeListener()
            {
                @Override
                public void onStopTrackingTouch(SeekBar arg0)
                {
                }

                @Override
                public void onStartTrackingTouch(SeekBar arg0)
                {
                }

                @Override
                public void onProgressChanged(SeekBar arg0, int progress, boolean arg2)
                {
                    audioManager.setStreamVolume(AudioManager.STREAM_MUSIC,
                            progress, 0);
                }
            });
        }
        catch (Exception e)
        {
            e.printStackTrace();
        }
    }

    private void back() {
        btn_back.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                navController.navigate(R.id.mainFragment);
            }
        });
    }

    private void setMenu() {

        menuView.setOnMenuItemClickListener(new ActionMenuView.OnMenuItemClickListener() {
            @Override
            public boolean onMenuItemClick(MenuItem menuItem) {

                return onOptionsItemSelected(menuItem);
            }
        });
    }

    private void sendRequestToUpdate(String token, UserInfoWithoutPhone model) {

        NetworkService.getService().updateUser(token, model, new RegisterCallBack() {
            @Override
            public void doRegister() {


                progressDialog.dismiss();
                edit.setClickable(true);

                Toast.makeText(getContext(), "ثبت اطلاعات با موفقیت انجام شد", Toast.LENGTH_SHORT).show();

                if (getGhange != null)
                    getGhange.isGhanged();
            }

            @Override
            public void notRegister(Throwable t) {

                Toast.makeText(getContext(), ""+ t.getMessage(), Toast.LENGTH_SHORT).show();
                edit.setClickable(true);
                progressDialog.dismiss();


                }

            @Override
            public void dublicateNumber() {
                checkUp = 0 ;
                Toast.makeText(getContext(), "شما قبلا با این شماره ثبت نام کردید", Toast.LENGTH_SHORT).show();
                edit.setClickable(true);
                progressDialog.dismiss();


            }
        });
    }

    private void sendRequestToSecondUpdate(String token, UserInfoWithoutPhone model) {

        NetworkService.getService().updateUser(token, model, new RegisterCallBack() {
            @Override
            public void doRegister() {

                edit.setClickable(true);
                progressDialog.dismiss();

                Bundle bundle = new Bundle();
               bundle.putString("uuid" , uuid);
                bundle.putString("phone" , phone.getText().toString());

                navController.navigate(R.id.secondRegisterCode , bundle);

               }

            @Override
            public void notRegister(Throwable t) {

                Toast.makeText(getContext(), ""+t.getMessage() , Toast.LENGTH_SHORT).show();
                edit.setClickable(true);
                progressDialog.dismiss();


            }

            @Override
            public void dublicateNumber() {

                Toast.makeText(getContext(), "شما قبلا با این شماره ثبت نام کردید", Toast.LENGTH_SHORT).show();
                edit.setClickable(true);
                progressDialog.dismiss();



            }
        });
    }

    private boolean checkSamplePermission() {
        int currentAPIVersion = Build.VERSION.SDK_INT;
        if (currentAPIVersion >= android.os.Build.VERSION_CODES.M) {
            if (ContextCompat.checkSelfPermission(getContext(), Manifest.permission.READ_EXTERNAL_STORAGE) != PackageManager.PERMISSION_GRANTED) {
                if (ActivityCompat.shouldShowRequestPermissionRationale(getActivity(), Manifest.permission.READ_EXTERNAL_STORAGE)) {
                    AlertDialog.Builder alertBuilder = new AlertDialog.Builder(getContext());
                    alertBuilder.setCancelable(true);
                    alertBuilder.setTitle("مجوز دسترسی");
                    alertBuilder.setMessage("برای ادامه نیاز به دسترسی به حافظه داریم");
                    alertBuilder.setPositiveButton(android.R.string.yes, new DialogInterface.OnClickListener() {
                        @TargetApi(Build.VERSION_CODES.JELLY_BEAN)
                        public void onClick(DialogInterface dialog, int which) {
                            if (ActivityCompat.checkSelfPermission(getContext(), Manifest.permission.CAMERA) != PackageManager.PERMISSION_GRANTED || ActivityCompat.checkSelfPermission(getActivity(), Manifest.permission.WRITE_EXTERNAL_STORAGE) != PackageManager.PERMISSION_GRANTED) {

                                if (ActivityCompat.checkSelfPermission(getContext(), Manifest.permission.CAMERA) != PackageManager.PERMISSION_GRANTED ||
                                        ActivityCompat.checkSelfPermission(getActivity(),
                                                Manifest.permission.WRITE_EXTERNAL_STORAGE) != PackageManager.PERMISSION_GRANTED) {
                                    ActivityCompat.requestPermissions(getActivity(), new String[]{Manifest.permission.CAMERA, Manifest.permission.WRITE_EXTERNAL_STORAGE}, 1);
                                } else {
                                    Intent camera = new Intent(android.provider.MediaStore.ACTION_IMAGE_CAPTURE);
                                    startActivityForResult(camera, 0);
                                }

                            }
                        }
                    });
                    AlertDialog alert = alertBuilder.create();
                    alert.show();

                } else {
                    ActivityCompat.requestPermissions(getActivity(), new String[]{Manifest.permission.READ_EXTERNAL_STORAGE}, 0);
                }
                return false;
            } else {
                return true;
            }
        } else {
            return true;
        }
    }


    private void getImageForEdit() {

        img_user.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                result = checkSamplePermission();


                if (result) {
                    final CharSequence[] options = {"Take Photo", "Choose from Gallery", "Cancel"};
                    AlertDialog.Builder builder = new AlertDialog.Builder(getContext());
                    builder.setTitle("Choose your Profile picture");
                    builder.setItems(options, new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialog, int item) {
                            if (options[item].equals("Take Photo")) {

                                if (ActivityCompat.checkSelfPermission(getContext(), Manifest.permission.CAMERA) != PackageManager.PERMISSION_GRANTED ||
                                        ActivityCompat.checkSelfPermission(getActivity(), Manifest.permission.WRITE_EXTERNAL_STORAGE) != PackageManager.PERMISSION_GRANTED) {
                                    ActivityCompat.requestPermissions(getActivity(), new String[]{Manifest.permission.CAMERA, Manifest.permission.WRITE_EXTERNAL_STORAGE}, 1);
                                } else {
                                    Intent camera = new Intent(android.provider.MediaStore.ACTION_IMAGE_CAPTURE);
                                    startActivityForResult(camera, 0);
                                }


                            } else if (options[item].equals("Choose from Gallery")) {
                                Intent pickPhoto = new Intent(Intent.ACTION_PICK, android.provider.MediaStore.Images.Media.EXTERNAL_CONTENT_URI);
                                startActivityForResult(pickPhoto, 1);
                            } else if (options[item].equals("Cancel")) {
                                dialog.dismiss();
                            }
                        }
                    });
                    builder.show();
                }
            }

        });
    }

    private void getUserInfo(String token) {

           progressDialog = new ProgressDialog(getContext(), R.style.AppTheme_Green_Dialog);
           progressDialog.setIndeterminate(true);
           progressDialog.setMessage("در حال دریافت... ");
           progressDialog.show();
           progressDialog.setCancelable(true);

                NetworkService.getService().userInformation(token, new UserInfoCallBack() {
                    @Override
                    public void userInfoIsAvailable(UserInfoModel infoList) {
//                        fillInfoAboutUse(infoList);

                        progressDialog.dismiss();
                    }

                    @Override
                    public void infoNotAvailable(Throwable t) {

                        progressDialog.dismiss();

                    }
                });
            }


    public void fillProfile(MutableLiveData<UserInfo> infoList) {

                sName.setText(Objects.requireNonNull(infoList.getValue()).getName());
                phone.setText(infoList.getValue().getPhone());
                age = infoList.getValue().getAge();

        String imguser = infoList.getValue().getPic();
        String newString = imguser.replace("https", "http");
                if (infoList.getValue().getPic() != null){
                    RequestOptions requestOptions = new RequestOptions().diskCacheStrategy(DiskCacheStrategy.ALL);
                    Glide.with(Objects.requireNonNull(getActivity()))
                           .load(newString)         .apply(requestOptions)         .into(img_user);
                   img_user.setRotation(0);


                }else {

                    img_user.setImageResource(R.drawable.update_prof);
                   img_user.setRotation(0);
               }

        phoneNumber = infoList.getValue().getPhone();

        Log.e("rt" ,"profile ischange" );
            }



            @Override
            public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {

                inflater.inflate(R.menu.profile_menu, menuView.getMenu());

            }


            @Override
            public boolean onOptionsItemSelected(MenuItem item) {


                if (item.getItemId() == R.id.menu_exit) {
                    initExit();
                    return true;
                }
                return super.onOptionsItemSelected(item);
            }

            private void initExit() {

                String token = SharePreferencesHelper.getInatance().getStringValueFromShare(getContext());
                NetworkService.getService().logOut(token, new LogOutCallBack() {
                    @Override
                    public void userIsLogout() {

                        SharePreferencesHelper.getInatance().insertString(getContext(), null);
                        Objects.requireNonNull(getActivity()).finishAffinity();

                    }

                    @Override
                    public void userNotLogout(Throwable t) {

                        Toast.makeText(getContext(), "لطفا دوباره امتحان کنید", Toast.LENGTH_SHORT).show();
                    }
                });

            }

            @Override
            public void onActivityResult(int requestCode, int resultCode, Intent data) {

                super.onActivityResult(requestCode, resultCode, data);


                if (resultCode == Activity.RESULT_OK) {
                    switch (requestCode) {
                        case 0:
                            if (data != null) {

                                // If selectedImageUri is null check extras for bitmap
                                Bitmap bmp = (Bitmap) data.getExtras().get("data");
                                img_user.setImageBitmap(bmp);


                                //create a file to write bitmap data
                                File f = new File(getContext().getCacheDir(), "fff.jpg");
                                try {
                                    f.createNewFile();
                                } catch (IOException e) {
                                    e.printStackTrace();
                                }
                               



                                bmp = getResizedBitmap(bmp, 100);
                                
                                ByteArrayOutputStream bos = new ByteArrayOutputStream();
                                bmp.compress(Bitmap.CompressFormat.PNG, 0 /*ignored for PNG*/, bos);
                                byte[] bitmapdata = bos.toByteArray();


                                FileOutputStream fos = null;
                                try {
                                    fos = new FileOutputStream(f);

                                } catch (FileNotFoundException e) {
                                    e.printStackTrace();
                                }
                                try {
                                    fos.write(bitmapdata);
                                } catch (IOException e) {
                                    e.printStackTrace();
                                }
                                try {
                                    fos.flush();
                                } catch (IOException e) {
                                    e.printStackTrace();
                                }
                                try {
                                    fos.close();
                                } catch (IOException e) {
                                    e.printStackTrace();
                                }

                                model.setPic(f);
                                isvalidPic =true;


                            }

                            break;
                        case 1:
                            if (data != null) {

                                Uri uri = data.getData();
                                img_user.setImageURI(uri);
                                img_user.setRotation(0);

                                File f = new File(getContext().getCacheDir(), "fs.jpg");
                                try {
                                    f.createNewFile();
                                } catch (IOException e) {
                                    e.printStackTrace();
                                }

                                try {
                                    Bitmap bitmap = MediaStore.Images.Media.getBitmap(getActivity().getContentResolver(), uri);
                                    bitmap = getResizedBitmap(bitmap, 120);


                                    ByteArrayOutputStream bos = new ByteArrayOutputStream();
                                    bitmap.compress(Bitmap.CompressFormat.PNG, 0 /*ignored for PNG*/, bos);
                                    byte[] bitmapdata = bos.toByteArray();


                                    FileOutputStream fos = null;
                                    try {
                                        fos = new FileOutputStream(f);

                                    } catch (FileNotFoundException e) {
                                        e.printStackTrace();
                                    }
                                    try {
                                        fos.write(bitmapdata);
                                    } catch (IOException e) {
                                        e.printStackTrace();
                                    }
                                    try {
                                        fos.flush();
                                    } catch (IOException e) {
                                        e.printStackTrace();
                                    }
                                    try {
                                        fos.close();
                                    } catch (IOException e) {
                                        e.printStackTrace();
                                    }

                                    model.setPic(f);
                                    isvalidPic =true;
                                } catch (IOException e) {
                                    e.printStackTrace();
                                }


//                                try {
//                                    InputStream is = getActivity().getContentResolver().openInputStream(uri);
//
//                                    byte[] buffer = new byte[8 * 1024];
//                                    File output = new File(getActivity().getCacheDir() + "mmm.jpg");
//
//
//                                    FileOutputStream os = new FileOutputStream(output);
//                                    int len;
//                                    while ((len = is.read(buffer)) > 0) {
//                                        os.write(buffer, 0, len);
//                                    }
//                                    os.close();
//                                    is.close();
//                                    Log.e("mkmk2", output.exists() + " " + output.getAbsolutePath());
//
//                                    model.setPic(output);
//
//                                    isvalidPic =true;
//                                }catch (IOException e){}

                            }
                            break;

                    }
                }
            }



    private Bitmap getResizedBitmap(Bitmap bitmap, int i) {

        int width = bitmap.getWidth();
        int height = bitmap.getHeight();

        float bitmapRatio = (float)width / (float) height;
        if (bitmapRatio > 1) {
            width = i;
            height = (int) (width / bitmapRatio);
        } else {
            height = i;
            width = (int) (height * bitmapRatio);
        }
        return Bitmap.createScaledBitmap(bitmap, width, height, true);
    }


}
