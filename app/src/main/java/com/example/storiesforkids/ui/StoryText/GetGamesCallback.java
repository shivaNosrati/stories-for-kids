package com.example.storiesforkids.ui.StoryText;

import com.example.storiesforkids.ui.Game.GameModel;

public interface GetGamesCallback {

    void gameReceived(GameModel data);

    void gameNotReceive(Throwable t);
}
