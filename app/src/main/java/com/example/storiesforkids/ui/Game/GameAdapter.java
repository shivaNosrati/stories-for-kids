package com.example.storiesforkids.ui.Game;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.bumptech.glide.Glide;
import com.example.storiesforkids.R;
import com.example.storiesforkids.ui.MainView.MainActivity;

import java.util.List;

public class GameAdapter extends RecyclerView.Adapter<GameAdapter.ViewHolder> {


    private List<GameData> dataList;
    private Context context;
    private ClickOnGameItem clickevent;


    public GameAdapter(List<GameData> dataList , Context context , GameFragment gameFragment) {

        this.dataList=dataList;
        this.context =context;
        this.clickevent = gameFragment;

    }

    @NonNull
    @Override
    public GameAdapter.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View v = LayoutInflater.from(parent.getContext()).inflate(R.layout.game_item, parent, false);
        GameAdapter.ViewHolder vh = new GameAdapter.ViewHolder(v);
        return vh;
    }

    @Override
    public void onBindViewHolder(@NonNull ViewHolder holder, int position) {
        String img = dataList.get(position).getGamePic();
        String newString = img.replace("https", "http");
        Glide.with(context).load(newString).into(holder.img_game);
        holder.textView.setText(dataList.get(position).getName());



        holder.img_game.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                clickevent.goToPlay(dataList.get(position));

            }
        });
    }



    @Override
    public int getItemCount() {
        return dataList.size();
    }


    public class ViewHolder extends RecyclerView.ViewHolder {

        private ImageView img_game;
        private TextView textView;


        public ViewHolder(View v) {
            super(v);

            img_game =  v.findViewById(R.id.img_game_item);
            textView = v.findViewById(R.id.gameName);

        }
    }

}
