package com.example.storiesforkids.ui.Service;

import com.example.storiesforkids.ui.Comments.CommentModel;
import com.example.storiesforkids.ui.Favorite.FavoritModel;
import com.example.storiesforkids.ui.History.HistoryModel;
import com.example.storiesforkids.ui.LoginRegister.CodeRegisterModel;
import com.example.storiesforkids.ui.LoginRegister.PhoneRegisterModel;
import com.example.storiesforkids.ui.MainFrg.InfoStoryModel;
import com.example.storiesforkids.ui.Game.GameModel;
import com.example.storiesforkids.ui.PlayVoice.ParagraphModel;
import com.example.storiesforkids.ui.UserInformation.UserInfoModel;
import com.example.storiesforkids.ui.Search.InfoSearchModel;

import okhttp3.MultipartBody;
import okhttp3.RequestBody;
import retrofit2.Call;
import retrofit2.http.Field;
import retrofit2.http.FormUrlEncoded;
import retrofit2.http.GET;
import retrofit2.http.Header;
import retrofit2.http.Multipart;
import retrofit2.http.POST;
import retrofit2.http.Part;
import retrofit2.http.Path;

public interface APIService {


    @POST("api/v1/register")
    @FormUrlEncoded
    Call<PhoneRegisterModel> userRegisterWithNum(@Field("phone") String phoneNumber ,
                                                 @Field("uu_id") String uu_id  );

    @POST("api/v1/register/verify")
    @FormUrlEncoded
    Call<CodeRegisterModel> userRegisterWithCode(@Field("phone") String phoneNumber ,
                                                 @Field("uu_id") String uu_id ,
                                                 @Field("code") Integer code );

    @GET("api/v1/stories")
    Call<InfoStoryModel> getDetails(
            @Header("Authorization") String token);

    @POST("api/v1/dislike/stories")
    @FormUrlEncoded
    Call<PhoneRegisterModel> setDisLike(
            @Field("story_id") Integer id
            , @Header("Authorization") String token);

    @POST("api/v1/favorite/stories")
    @FormUrlEncoded
    Call<PhoneRegisterModel> setFav(@Field("story_id") Integer id ,
                                    @Header("Authorization") String token);

    @GET("api/v1/favorites/stories")
    Call<FavoritModel> getFaoriteStory(
            @Header("Authorization") String token);


    @GET("api/v1/stories/{story_id}/similar")
    Call<InfoStoryModel> getSimilorStories(@Path("story_id") Integer id ,
                                          @Header("Authorization") String token);

    @POST("api/v1/history/stories")
    @FormUrlEncoded
    Call<PhoneRegisterModel> setHistory(@Field("story_id") Integer id ,
                                    @Header("Authorization") String token);



    @GET("api/v1/histories/stories")
    Call<HistoryModel> getHistory(
            @Header("Authorization") String token);


   @POST("api/v1/user/edit")
   @Multipart
   Call<PhoneRegisterModel> setuser (@Header("Authorization") String token,
                                     @Part("name") RequestBody name ,
                                     @Part("age") RequestBody age ,
                                     @Part("uu_id") RequestBody uu_id );
//                                     @Part MultipartBody.Part pic);

    @POST("api/v1/logout")
    Call<PhoneRegisterModel> logOut(
            @Header("Authorization") String token);


    @POST("api/v1/user/info")
    Call<UserInfoModel> userInfo(
            @Header("Authorization") String token);


    @POST("api/v1/user/edit")
    @Multipart
    Call<PhoneRegisterModel> updateUser (@Header("Authorization") String token, @Part("name") RequestBody name , @Part("age") RequestBody age , @Part("uu_id") RequestBody uu_id , @Part MultipartBody.Part pic ,@Part("phone") RequestBody phone);



    @POST("api/v1/update/verify")
    @FormUrlEncoded
    Call<CodeRegisterModel> updateVerification (@Header("Authorization") String token,
                                            @Field("phone") String phoneNumber ,
                                            @Field("uu_id") String uu_id ,
                                            @Field("code") Integer code );


    @GET("api/v1/stories/{story_id}/comments")
    Call<CommentModel> getComments(@Path("story_id") Integer id ,
                                   @Header("Authorization") String token);

    @POST("api/v1/comment/create")
    @FormUrlEncoded
    Call<PhoneRegisterModel> setComment(@Field("story_id") Integer id ,
                                        @Field("body") String body ,
                                    @Header("Authorization") String token);



    @GET("api/v1/games")
    Call<GameModel> getGames(
            @Header("Authorization") String token);


    @FormUrlEncoded
    @POST("api/v1/stories/search")
    Call<InfoSearchModel> getResponseSearch(@Field("search") String search ,
                                                  @Header("Authorization") String token);

    @POST("api/v1/stories/paragraph")
    @FormUrlEncoded
    Call<ParagraphModel> getParagraph(
            @Field("story_id")Integer sId ,
            @Header("Authorization") String token);
}
