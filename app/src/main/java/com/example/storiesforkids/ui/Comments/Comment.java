package com.example.storiesforkids.ui.Comments;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class Comment {

       @SerializedName("body")
       @Expose
        private String body;


        @SerializedName("user_name")
        @Expose
        private String userName;


        @SerializedName("user_pic")
        @Expose
        private String user_pic;


        @SerializedName("created_at")
        @Expose
        private String created_at;


        public String getUserName() {
            return userName;
        }

        public void setUserName(String userName) {
            this.userName = userName;
        }


    public String getBody() {
        return body;
    }

    public String getCreated_at() {
        return created_at;
    }

    public void setBody(String body) {
        this.body = body;
    }

    public void setCreated_at(String created_at) {
        this.created_at = created_at;
    }

    public String getUser_pic() {
        return user_pic;
    }

    public void setUser_pic(String user_pic) {
        this.user_pic = user_pic;
    }
}

