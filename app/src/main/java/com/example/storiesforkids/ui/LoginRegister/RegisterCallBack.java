package com.example.storiesforkids.ui.LoginRegister;

public interface RegisterCallBack {

    void doRegister();
    void notRegister( Throwable t);
    void dublicateNumber();
}
