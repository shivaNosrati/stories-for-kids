package com.example.storiesforkids.ui.Comments;

import android.content.Context;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.bumptech.glide.Glide;
import com.example.storiesforkids.R;
import com.example.storiesforkids.ui.MainView.MainActivity;

import java.util.List;

import de.hdodenhof.circleimageview.CircleImageView;

public class CommentAdapter extends RecyclerView.Adapter<CommentAdapter.ViewHolder> {


        private List<Comment> cList;
        private Context context;
//    private ClickOnFaveImg clickOnFaveImg;


        public CommentAdapter(List<Comment> cList , Context context , MainActivity mainActivity) {

            this.cList=cList;
            this.context =context;
//        this.clickOnFaveImg = mainActivity;

        }

        @NonNull
        @Override
        public CommentAdapter.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
            View v = LayoutInflater.from(parent.getContext()).inflate(R.layout.comment_item, parent, false);
            CommentAdapter.ViewHolder vh = new CommentAdapter.ViewHolder(v);
            return vh;
        }

        @Override
        public void onBindViewHolder(@NonNull ViewHolder holder, int position) {

            if (cList.get(position).getUser_pic() == null){

                holder.userImageBlank.setVisibility(View.VISIBLE);
                holder.userImage.setVisibility(View.GONE);
                holder.comments.setText(cList.get(position).getBody());

            }else {

                String img = cList.get(position).getUser_pic();
                String newString = img.replace("https", "http");
                holder.comments.setText(cList.get(position).getBody());
                Glide.with(context).load(newString).into(holder.userImage);
                holder.userImage.setRotation(0);
            }


        }



        @Override
        public int getItemCount() {
            return cList.size();
        }


        public class ViewHolder extends RecyclerView.ViewHolder {

            private TextView comments;
            private CircleImageView userImage;
            private CircleImageView userImageBlank;


            public ViewHolder(View v) {
                super(v);

                comments =  v.findViewById(R.id.txt_comment);
                userImage= v.findViewById(R.id.profileComment);
                userImageBlank = v.findViewById(R.id.profileComment_blank);
              userImage.setRotation(90);

            }
        }
    }

