package com.example.storiesforkids.ui.LoginRegister;

import android.annotation.SuppressLint;
import android.app.ProgressDialog;
import android.content.Context;
import android.os.Build;
import android.os.Bundle;
import android.provider.Settings;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.annotation.RequiresApi;
import androidx.fragment.app.Fragment;
import androidx.navigation.NavController;
import androidx.navigation.Navigation;

import com.example.storiesforkids.R;
import com.example.storiesforkids.ui.CheckNetwork.CheckNetworkConnectionUtils;
import com.example.storiesforkids.ui.CheckNetwork.INetworkCallBack;
import com.example.storiesforkids.ui.Service.NetworkService;
import com.example.storiesforkids.ui.Share.SharePreferencesHelper;

import java.util.Objects;

import butterknife.BindView;
import butterknife.ButterKnife;


public class PhoneRegister extends Fragment {

    @BindView(R.id.edt_enter_number)
    EditText phoneNumber;
    @BindView(R.id.ok_number)
    Button registerButton;
    private boolean hasInternet = false;
    ProgressDialog progressDialog;

    private NavController navController;

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view =inflater.inflate(R.layout.activity_phone_register , container , false);
        ButterKnife.bind(this, view);

        navController = Navigation.findNavController(getActivity(), R.id.nav_login_host);
        ((BaseLoginActivity)getActivity()).updateStatusBarColor("#CD0202");
        CheckNetworkConnectionUtils.getInstance().checkHasInternet(getContext(), new INetworkCallBack() {

            @Override
            public void hasInternet() {
                hasInternet = true;
            }

            @Override
            public void noInternet() {
                hasInternet=true;

            }
        });
        return view;

    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        registerButton.setOnClickListener(new View.OnClickListener() {
            @RequiresApi(api = Build.VERSION_CODES.M)
            @Override
            public void onClick(View v) {

                if (hasInternet){

                    registerButton.setClickable(true);
                    Register();
                }
                else
                    Toast.makeText(getActivity(), "عدم برقراری ارتباط با سرور", Toast.LENGTH_SHORT).show();


            }
        });
    }

    private void Register() {
        if (!validate()) {
            return;
        }

        progressDialog = new ProgressDialog(getContext(),R.style.AppTheme_Green_Dialog);
        progressDialog.setIndeterminate(true);
        progressDialog.setMessage("در حال دریافت... ");
        progressDialog.show();
        progressDialog.setCancelable(false);

        String phone = phoneNumber.getText().toString();
        @SuppressLint("HardwareIds")
        String androidId = Settings.Secure.getString(Objects.requireNonNull(getContext()).getContentResolver(),
                Settings.Secure.ANDROID_ID);

        SharePreferencesHelper.getInatance().insertUuidtString(getContext() , androidId);

        sendRequestRegister(phone, androidId);
    }

    private void sendRequestRegister(String phone, String uuid) {

        NetworkService.getService().registerWithPhone(phone, uuid, new RegisterCallBack() {
            @Override
            public void doRegister() {
                progressDialog.dismiss();
                registerButton.setClickable(true);
                Bundle dataBundle = new Bundle();
                dataBundle.putString("phone" ,phone);
                dataBundle.putString("uuid" ,uuid);

                navController.navigate(R.id.codeRegister1 , dataBundle);
            }

            @Override
            public void notRegister(Throwable t) {

                progressDialog.dismiss();
                registerButton.setClickable(true);
                Toast.makeText(getContext() , ""+t.getMessage() , Toast.LENGTH_SHORT).show();
            }

            @Override
            public void dublicateNumber() {
                registerButton.setClickable(true);
                progressDialog.dismiss();
                Toast.makeText(getContext() , "خطای عدم دسترسی", Toast.LENGTH_SHORT).show();
            }
        });


    }

    public boolean validate() {
        boolean valid = true;

        String phone = phoneNumber.getText().toString();

        if (phone.isEmpty() || phone.length() < 11 ) {
            phoneNumber.setError("شماره تلفن معتبری وارد نمایید");
            valid = false;
        } else {
            phoneNumber.setError(null);
        }


        return valid;
    }


}
