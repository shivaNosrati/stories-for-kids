package com.example.storiesforkids.ui.UserInformation;

public interface UserInfoCallBack {

    void userInfoIsAvailable(UserInfoModel infoList);

    void infoNotAvailable(Throwable t);
}
