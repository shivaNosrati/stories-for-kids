package com.example.storiesforkids.ui.UserInformation;

import android.app.ProgressDialog;
import android.content.Context;
import android.os.Build;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.annotation.RequiresApi;
import androidx.fragment.app.Fragment;
import androidx.navigation.NavController;
import androidx.navigation.Navigation;

import com.example.storiesforkids.R;
import com.example.storiesforkids.ui.CheckNetwork.CheckNetworkConnectionUtils;
import com.example.storiesforkids.ui.CheckNetwork.INetworkCallBack;
import com.example.storiesforkids.ui.LoginRegister.VerifyCallBack;
import com.example.storiesforkids.ui.MainView.MainActivity;
import com.example.storiesforkids.ui.Service.NetworkService;
import com.example.storiesforkids.ui.Share.SharePreferencesHelper;

import butterknife.BindView;
import butterknife.ButterKnife;

public class SecondRegisterCode extends Fragment {

    @BindView(R.id.second_edt_enter_code)
    EditText enterCode;

    @BindView(R.id.second_btn_ok_code)
    Button registerButton;


    private Integer iCode;
    private NavController navController;
    ProgressDialog progressDialog;
    private boolean hasInternet = false;


    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {

        View view = inflater.inflate(R.layout.second_register , container , false);

        navController = Navigation.findNavController(getActivity(), R.id.nav_host_fragment);

        ((MainActivity)getActivity()).updateStatusBarColor("#CD0202");
        ButterKnife.bind(this, view);

        CheckNetworkConnectionUtils.getInstance().checkHasInternet(getContext(), new INetworkCallBack() {

            @Override
            public void hasInternet() {
                hasInternet = true;

            }

            @Override
            public void noInternet() {
                hasInternet=true;

            }
        });

        return view;
    }


    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);


        registerButton.setOnClickListener(new View.OnClickListener() {
            @RequiresApi(api = Build.VERSION_CODES.M)
            @Override
            public void onClick(View v) {

                if (hasInternet){
                    Register();
                }
                else
                    hasInternet=true;


            }
        });
    }

    private void Register() {
        if (!validate()) {
            return;
        }
        registerButton.setClickable(false);
        progressDialog = new ProgressDialog(getContext(),R.style.AppTheme_Green_Dialog);
        progressDialog.setIndeterminate(true);
        progressDialog.setMessage("صبور باشید... ");
        progressDialog.show();
        progressDialog.setCancelable(false);

        String phoneNumber = getArguments().getString("phone");
        String uuId = getArguments().getString("uuid");
        String token = SharePreferencesHelper.getInatance().getStringValueFromShare(getContext());

        if (phoneNumber != null && uuId != null) {

            iCode =Integer.valueOf(enterCode.getText().toString());
            sendRequestSecondRegister(token,phoneNumber, uuId, iCode);
        }

    }

    private void sendRequestSecondRegister(String token , String phoneNumber, String uuId, Integer iCode) {

        NetworkService.getService().updateVerif(token, phoneNumber, uuId, iCode, new VerifyCallBack() {
            @Override
            public void onReceiveToken(String token) {

                progressDialog.dismiss();


                storeToken("Bearer "+token);
                registerButton.setClickable(true);

                navController.navigate(R.id.mainFragment);

            }

            @Override
            public void notReceive() {
                registerButton.setClickable(true);
                progressDialog.dismiss();
                Toast.makeText(getContext(), "لطفا مجددا تلاش کنید", Toast.LENGTH_SHORT).show();
            }

            @Override
            public void onDublicate() {
                registerButton.setClickable(true);
                progressDialog.dismiss();
                Toast.makeText(getContext(), "کد وارد شده صحیح نیست", Toast.LENGTH_SHORT).show();
            }
        });
    }


    private void storeToken(String token) {

        SharePreferencesHelper.getInatance().insertString(getContext(),token);
    }

    public boolean validate() {
        boolean valid = true;

        String code = enterCode.getText().toString();

        if (code.isEmpty()) {
            enterCode.setError("کد وارد نمایید");
            registerButton.setClickable(true);
            valid = false;
        } else if (code.length() !=5){

            registerButton.setClickable(true);
            enterCode.setError("کد پنج رقمی است");
            valid=false;

        }
        else
            enterCode.setError(null);

        return valid;
    }
}
