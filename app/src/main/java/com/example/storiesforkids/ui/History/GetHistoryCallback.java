package com.example.storiesforkids.ui.History;


public interface GetHistoryCallback {

    void historyIsSet(HistoryModel historyModel);

    void historyNotSet(Throwable t);
}
