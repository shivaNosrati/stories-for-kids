package com.example.storiesforkids.ui.Service;

import android.content.Context;

import com.example.storiesforkids.ui.AllOfStoryInfo.DisLikeCallback;
import com.example.storiesforkids.ui.AllOfStoryInfo.SetFaveCallback;
import com.example.storiesforkids.ui.Comments.GetCommentsCallback;
import com.example.storiesforkids.ui.Favorite.GetFaveCallBack;
import com.example.storiesforkids.ui.History.GetHistoryCallback;
import com.example.storiesforkids.ui.History.SetHistoryCallback;
import com.example.storiesforkids.ui.LoginRegister.LogOutCallBack;
import com.example.storiesforkids.ui.LoginRegister.RegisterCallBack;
import com.example.storiesforkids.ui.LoginRegister.VerifyCallBack;
import com.example.storiesforkids.ui.MainFrg.GetStoriesCallback;
import com.example.storiesforkids.ui.Search.ResponsesSearch;
import com.example.storiesforkids.ui.Simolor.GetSimilorsCallback;
import com.example.storiesforkids.ui.StoryText.GetGamesCallback;
import com.example.storiesforkids.ui.PlayVoice.Paragraphcallback;
import com.example.storiesforkids.ui.UserInformation.UserInfoCallBack;
import com.example.storiesforkids.ui.UserInformation.UserInfoWithoutPhone;
import com.example.storiesforkids.ui.UserInformation.UserInformationModel;
import com.example.storiesforkids.ui.Utils.DoSomeThingInRequest;

public interface INetworkServices {

    void registerWithPhone(String phoneNumber , String uuId, RegisterCallBack callBack);

    void registerWithCode(String phoneNumber , String uuId, Integer code , VerifyCallBack callBack);

    void getStories( String token , GetStoriesCallback callback );

    void setFav(Integer id ,String token, SetFaveCallback callback);

    void disLike(Integer id ,String token, DisLikeCallback callback);

    void getFav(String token , GetFaveCallBack callback);

    void getSim(Integer id , String token ,GetSimilorsCallback callback);

    void setHis(Integer id ,String token, SetHistoryCallback callback);

    void getHis(String token, GetHistoryCallback callback);

    void getsearchresponse(String strSearch,String token, ResponsesSearch callBack);

    void setProfile(String token , UserInformationModel model , RegisterCallBack callback);

    void logOut (String token , LogOutCallBack callBack );

    void userInformation(String token , UserInfoCallBack callback);

    void updateUser(String token , UserInfoWithoutPhone model , RegisterCallBack callback);

    void updateVerif(String token , String phone , String uuid , Integer code , VerifyCallBack callback);

    void userComment(Integer id ,String token , GetCommentsCallback callback);

    void setComment(String body,Integer id, String token , SetFaveCallback callback);

    void getGames(String token , GetGamesCallback callback);

    void getParagraph(String token , Integer id , Paragraphcallback callback);

}
