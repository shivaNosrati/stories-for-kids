package com.example.storiesforkids.ui.LoginRegister;

import android.app.ProgressDialog;
import android.content.Context;
import android.os.Build;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.annotation.RequiresApi;
import androidx.fragment.app.Fragment;
import androidx.navigation.NavController;
import androidx.navigation.NavOptions;

import com.example.storiesforkids.R;
import com.example.storiesforkids.ui.CheckNetwork.CheckNetworkConnectionUtils;
import com.example.storiesforkids.ui.CheckNetwork.INetworkCallBack;
import com.example.storiesforkids.ui.Service.NetworkService;
import com.example.storiesforkids.ui.Share.SharePreferencesHelper;
import com.example.storiesforkids.ui.UserInformation.UserInformationModel;

import java.util.Objects;

import butterknife.BindView;
import butterknife.ButterKnife;

import static androidx.navigation.Navigation.findNavController;

public class SetProfileFragment extends Fragment {

    private static int RESULT_LOAD_IMAGE = 1;
    private boolean hasInternet = false;
    ProgressDialog progressDialog;


    @BindView(R.id.edt_setName)
    EditText setName;

    @BindView(R.id.edt_setAge)
    EditText setAge;

    @BindView(R.id.btn_ok_setProf)
    Button okProfile;

    private NavController navController;

    final int PERMISSION_REQUEST_STORAGE = 1234;

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View v = inflater.inflate(R.layout.set_profile, container, false);
        ButterKnife.bind(this, v);
        navController = findNavController(getActivity(), R.id.nav_login_host);
        CheckNetworkConnectionUtils.getInstance().checkHasInternet(getContext(), new INetworkCallBack() {

            @Override
            public void hasInternet() {
                hasInternet = true;
            }

            @Override
            public void noInternet() {
                hasInternet = true;
                Toast.makeText(getActivity(), "عدم برقراری ارتباط با سرور", Toast.LENGTH_SHORT).show();
            }
        });


        return v;
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);


        okProfile.setOnClickListener(new View.OnClickListener() {
            @Override
            @RequiresApi(api = Build.VERSION_CODES.M)
            public void onClick(View view) {

                if (hasInternet) {
                    Register();

                } else
                    hasInternet = true;
            }
        });

    }

    private void Register() {

        if (!validatee()) {
            return;
        }
        okProfile.setClickable(false);
        progressDialog = new ProgressDialog(getContext(), R.style.AppTheme_Green_Dialog);
        progressDialog.setIndeterminate(true);
        progressDialog.setMessage("صبور باشید... ");
        progressDialog.show();
        progressDialog.setCancelable(false);

        String token = SharePreferencesHelper.getInatance().getStringValueFromShare(getContext());

        UserInformationModel userModel = new UserInformationModel();
        userModel.setAge(Integer.valueOf(setAge.getText().toString()));
        userModel.setName(setName.getText().toString());
        userModel.setUu_id(getArguments().getString("uuidUser"));
//
//        if (uri!= null){
//            Uri uri1 = Uri.parse(uri);
//            try {
//                InputStream is = getActivity().getContentResolver().openInputStream(uri1);
//
//                byte[] buffer = new byte[8 * 1024];
//                File output = new File(getActivity().getCacheDir() + "s.jpg");
//
//                FileOutputStream os = new FileOutputStream(output);
//                int len;
//                while ((len = is.read(buffer)) > 0) {
//                    os.write(buffer, 0, len);
//                }
//                os.close();
//                is.close();
//                Log.e("mkmk2", output.exists() + " " + output.getAbsolutePath());
////                profileModel.setPic(output);
//            }catch (IOException e){}
//            sendRequestSetProfile(token , profileModel);
//        }
//        else

        sendRequestSetProfile(token, userModel);
    }


    private void sendRequestSetProfile(String token, UserInformationModel profileModel) {
        NetworkService.getService().setProfile(token, profileModel, new RegisterCallBack() {
            @Override
            public void doRegister() {
                progressDialog.dismiss();
                okProfile.setClickable(true);
                navController.navigate(R.id.mainActivity, null, new NavOptions.Builder().setPopUpTo(R.id.codeRegister1, true).build());
                Objects.requireNonNull(getActivity()).onBackPressed();
            }

            @Override
            public void notRegister(Throwable t) {
                progressDialog.dismiss();
                okProfile.setClickable(true);
            }

            @Override
            public void dublicateNumber() {
                progressDialog.dismiss();
                okProfile.setClickable(true);

            }
        });
    }

    public boolean validatee() {

        boolean valid = true;

        String name = setName.getText().toString();
        String age = setAge.getText().toString();

        if (name.isEmpty()) {
            setName.setError("نام وارد نمایید");
            okProfile.setClickable(true);
            valid = false;

        }
        if (age.isEmpty()) {
            setAge.setError("سن وارد نمایید");
            okProfile.setClickable(true);
            valid = false;

        } else


            setName.setError(null);
        setAge.setError(null);
        okProfile.setClickable(true);
        return valid;
    }


}
