package com.example.storiesforkids.ui.StoryText;

import android.content.Context;
import android.graphics.drawable.Drawable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.FrameLayout;
import android.widget.ImageView;

import androidx.annotation.Nullable;

import com.bumptech.glide.Glide;
import com.bumptech.glide.load.DataSource;
import com.bumptech.glide.load.engine.DiskCacheStrategy;
import com.bumptech.glide.load.engine.GlideException;
import com.bumptech.glide.request.RequestListener;
import com.bumptech.glide.request.RequestOptions;
import com.bumptech.glide.request.target.Target;
import com.example.storiesforkids.R;
import com.example.storiesforkids.ui.PlayVoice.ParagraphData;
import com.smarteist.autoimageslider.SliderViewAdapter;

import java.util.List;

public class SliderAdapterStoryText extends SliderViewAdapter<SliderAdapterStoryText.SliderAdapterVH> {

    Animation animation;
    ImageView imageView;
    String url;
    private List<ParagraphData> tList;
    Context context;


    public SliderAdapterStoryText(List<ParagraphData> tList, Context context) {

        this.context = context;
        this.tList = tList;
    }


    @Override
    public SliderAdapterVH onCreateViewHolder(ViewGroup parent) {
        View inflate = LayoutInflater.from(parent.getContext()).inflate(R.layout.image_slider_layout_item, null);
        animation = AnimationUtils.loadAnimation(context, R.anim.rotate_animation);
        animation.setRepeatCount(Animation.INFINITE);

        return new SliderAdapterVH(inflate);
    }

    @Override
    public void onBindViewHolder(SliderAdapterVH viewHolder, int position) {

        RequestOptions requestOptions = new RequestOptions().diskCacheStrategy(DiskCacheStrategy.ALL);
        imageView = viewHolder.imageViewBackground;
        url = tList.get(position).getPic();


        viewHolder.progressBarHolder.setVisibility(View.VISIBLE);
        viewHolder.progressBar.setVisibility(View.VISIBLE);
        viewHolder.progressBar.startAnimation(animation);



        String newString = url.replace("https", "http");

        Glide.with(viewHolder.itemView)
                .load(newString)
                .thumbnail(0.5f)
                .addListener(new RequestListener<Drawable>() {

                    @Override
                    public boolean onLoadFailed(@Nullable GlideException e, Object model, Target<Drawable> target, boolean isFirstResource) {
                        return false;
                    }

                    @Override
                    public boolean onResourceReady(Drawable resource, Object model, Target<Drawable> target, DataSource dataSource, boolean isFirstResource) {
                        viewHolder.progressBarHolder.setVisibility(View.GONE);
                        viewHolder.progressBar.setVisibility(View.GONE);
                        animation.cancel();

                        return false;
                    }
                })
                .fitCenter()
                .apply(requestOptions)
                .into(imageView);


    }

    @Override
    public int getCount() {
        //slider view count could be dynamic size
        return tList.size();
    }

    class SliderAdapterVH extends SliderViewAdapter.ViewHolder {

        View itemView;
        ImageView imageViewBackground;
        ImageView progressBar;

        FrameLayout progressBarHolder;


        public SliderAdapterVH(View itemView) {
            super(itemView);
            imageViewBackground = itemView.findViewById(R.id.iv_auto_image_slider);

            progressBar = itemView.findViewById(R.id.progress_t);
            progressBarHolder = itemView.findViewById(R.id.progressBarHolder_t);



            this.itemView = itemView;

        }
    }


}
