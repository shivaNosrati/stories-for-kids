package com.example.storiesforkids.ui.LoginRegister;


import android.content.Intent;
import android.graphics.Color;
import android.os.Build;
import android.os.Bundle;
import android.view.Window;
import android.view.WindowManager;

import com.example.storiesforkids.R;
import com.example.storiesforkids.ui.BaseActivity;
import com.example.storiesforkids.ui.MainView.MainActivity;
import com.example.storiesforkids.ui.Share.SharePreferencesHelper;

public class BaseLoginActivity extends BaseActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_base_login);


        String token = SharePreferencesHelper.getInatance().getStringValueFromShare(this);
        if (token != null)
            startActivity(new Intent(BaseLoginActivity.this, MainActivity.class));

    }



    public void updateStatusBarColor(String color) {// Color must be in hexadecimal fromat
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            Window window = getWindow();
            window.addFlags(WindowManager.LayoutParams.FLAG_DRAWS_SYSTEM_BAR_BACKGROUNDS);
            window.setStatusBarColor(Color.parseColor(color));
        }
    }

}
