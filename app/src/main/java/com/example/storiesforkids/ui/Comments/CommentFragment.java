package com.example.storiesforkids.ui.Comments;

import android.app.ProgressDialog;
import android.content.Context;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.navigation.NavController;
import androidx.navigation.Navigation;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import androidx.swiperefreshlayout.widget.SwipeRefreshLayout;

import com.example.storiesforkids.R;
import com.example.storiesforkids.ui.AllOfStoryInfo.SetFaveCallback;
import com.example.storiesforkids.ui.MainView.MainActivity;
import com.example.storiesforkids.ui.Service.NetworkService;
import com.example.storiesforkids.ui.Share.SharePreferencesHelper;
import com.example.storiesforkids.ui.Utils.UtilKeyboard;

import java.util.List;
import java.util.Objects;

import butterknife.BindView;
import butterknife.ButterKnife;


public class CommentFragment extends Fragment {

    @BindView(R.id.bComment)
    ImageView back;

    @BindView(R.id.recycler_comments)
    RecyclerView recyclerView;

    @BindView(R.id.img_send_comment)
    ImageView sendComment;

    @BindView(R.id.my_comment)
    EditText myCommentText;

    @BindView(R.id.refresh)
    SwipeRefreshLayout refreshLayout;

    private NavController navController;
    private  CommentAdapter adapter;
   private ProgressDialog progressDialog;

    private OnFragmentComment  uiListener;


    private Integer id;
    private String nameS;
    private String img;
    private String n;
    private String t;
    private String voiceUri;
    private Boolean isLiked;
    private static String file_url = "";


    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {


        View view = inflater.inflate(R.layout.fragment_comment , container , false);
        ButterKnife.bind(this , view);
        navController = Navigation.findNavController(getActivity(), R.id.nav_host_fragment);
        ((MainActivity)getActivity()).updateStatusBarColor("#AB13B4");

        return view;
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        getDetails();
        initBack();
        initGetComments();
        initSendComment();
        initRefresh();
        initShowKeyboard();

        progressDialog = new ProgressDialog(getContext(),R.style.AppTheme_Green_Dialog);
        progressDialog.setIndeterminate(true);
        progressDialog.setMessage("در حال دریافت... ");
        progressDialog.show();
        progressDialog.setCancelable(true);

    }

    private void getDetails() {
        voiceUri = Objects.requireNonNull(getArguments()).getString("voice");
        id = Integer.valueOf(Objects.requireNonNull(getArguments().get("id")).toString());
        nameS = Objects.requireNonNull(getArguments().get("storyName")).toString();
        img = Objects.requireNonNull(getArguments().get("img")).toString();
        n = Objects.requireNonNull(getArguments().get("writer")).toString();
        t = Objects.requireNonNull(getArguments().get("designer")).toString();
        file_url = Objects.requireNonNull(getArguments().get("voice")).toString();
        isLiked = getArguments().getBoolean("isLiked");
    }

    private void initShowKeyboard() {
        myCommentText.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                UtilKeyboard.show(getActivity());
            }
        });
    }

    private void initRefresh() {
        refreshLayout.setColorSchemeColors(getResources().getColor(R.color.colorAccent));
        refreshLayout.setColorScheme(R.color.colorAccent);
        refreshLayout.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh() {
                initGetComments();
                refreshLayout.setRefreshing(false);

            }
        });
    }

    private void initSendComment() {

        Integer id = Integer.valueOf(getArguments().get("id").toString());
        String token = SharePreferencesHelper.getInatance().getStringValueFromShare(getContext());
        sendComment.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                UtilKeyboard.hideKeyboard(getActivity());

                String body = myCommentText.getText().toString();


                if (body.equals("")){
                    myCommentText.setError("نظر خود را وارد نمایید");
                    sendComment.setClickable(false);
                }else

                            sendComment.setClickable(true);
                            NetworkService.getService().setComment(body, id, token, new SetFaveCallback() {
                                @Override
                                public void storyIsFave() {

                                    sendComment.setClickable(true);
                                    myCommentText.setText("");
                                    initGetComments();
                                }

                                @Override
                                public void storyNotSetToFave(Throwable t) {

                                    Toast.makeText(getContext(), "خطایی رخ داده است بعدا امتحان کنید", Toast.LENGTH_SHORT).show();
                                    sendComment.setClickable(true);
                                    myCommentText.setText("");
                                }
                            });

            }
        });

    }


    private void initGetComments() {

        String token = SharePreferencesHelper.getInatance().getStringValueFromShare(getContext());
        NetworkService.getService().userComment(id, token, new GetCommentsCallback() {
            @Override
            public void onReceive(CommentModel commentModel) {
                fillCommentsList(commentModel.getData());
                progressDialog.dismiss();
            }

            @Override
            public void notReceive(Throwable t) {
                progressDialog.dismiss();
            }
        });
    }

    private void fillCommentsList(List<Comment> data) {
        adapter = new CommentAdapter(data , getContext() , (MainActivity) getActivity());
        RecyclerView.LayoutManager layoutManager = new LinearLayoutManager(getActivity());
        recyclerView.setLayoutManager(layoutManager);
        recyclerView.setAdapter(adapter);
    }

    private void initBack() {
        back.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                Bundle dataBundle = new Bundle();
                dataBundle.putInt("id" ,id);
                dataBundle.putString("designer" ,t);
                dataBundle.putString("storyName" ,nameS);
                dataBundle.putString("writer" ,n);
                dataBundle.putString("img" ,img);
                dataBundle.putString("voice" , file_url);
                dataBundle.putBoolean("isLiked" , isLiked);

                navController.navigate(R.id.detailsStoryFragment , dataBundle);

            }
        });
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);

        if(context instanceof OnFragmentComment)
            this.uiListener= (OnFragmentComment) context;

    }


    public interface OnFragmentComment {

        void deleteSomeUI();
    }




}
