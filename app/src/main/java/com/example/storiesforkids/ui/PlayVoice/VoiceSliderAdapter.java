package com.example.storiesforkids.ui.PlayVoice;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import com.bumptech.glide.Glide;
import com.example.storiesforkids.R;
import com.smarteist.autoimageslider.SliderViewAdapter;


import java.util.List;


public class VoiceSliderAdapter extends SliderViewAdapter<VoiceSliderAdapter.SliderAdapterVH> {

    private List<ParagraphData> pList;
    Context context;

    public VoiceSliderAdapter(List<ParagraphData> pList , Context context) {

        this.context =context;
        this.pList = pList;
    }


    @Override
    public SliderAdapterVH onCreateViewHolder(ViewGroup parent) {
        View inflate = LayoutInflater.from(parent.getContext()).inflate(R.layout.image_slider_layout_item, null);
        return new SliderAdapterVH(inflate);
    }

    @Override
    public void onBindViewHolder(SliderAdapterVH viewHolder, int position) {

        String img = pList.get(position).getPic();
        String newString = img.replace("https", "http");
        Glide.with(viewHolder.itemView)
                .load(newString)
                .into(viewHolder.imageViewBackground);


    }

    @Override
    public int getCount() {
        //slider view count could be dynamic size
        return pList.size();
    }

    class SliderAdapterVH extends SliderViewAdapter.ViewHolder {

        View itemView;
        ImageView imageViewBackground;


        public SliderAdapterVH(View itemView) {
            super(itemView);
            imageViewBackground = itemView.findViewById(R.id.iv_auto_image_slider);
            this.itemView = itemView;
        }
    }
}
