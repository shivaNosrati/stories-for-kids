package com.example.storiesforkids.ui.Search;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.constraintlayout.widget.ConstraintLayout;
import androidx.recyclerview.widget.RecyclerView;

import com.bumptech.glide.Glide;
import com.example.storiesforkids.R;
import com.example.storiesforkids.ui.MainFrg.ClickOnButtonInterface;
import com.example.storiesforkids.ui.MainFrg.ClickOnButtonListen;
import com.example.storiesforkids.ui.MainFrg.ClickOnButtonRead;
import com.example.storiesforkids.ui.MainFrg.InfoStory;
import com.example.storiesforkids.ui.MainView.MainActivity;

import java.util.List;

public class SearchAdapter extends RecyclerView.Adapter<SearchAdapter.SearchViewHolder> {

    private List<InfoSearch> dataList;
    private Context context;
    private ClickOnSearchItem clickOnSearchItem;
    private ClickOnReadSearch clickOnReadSearch;
    private ClickOnVoiceSearch clickOnVoiceSearch;



    public SearchAdapter(List<InfoSearch> dataList , Context context , SearchFragment searchFragment) {

        this.dataList=dataList;
        this.context =context;
        this.clickOnSearchItem = searchFragment;
       this.clickOnReadSearch =  searchFragment;
        this.clickOnVoiceSearch =  searchFragment;


    }

    @NonNull
    @Override
    public SearchViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View v = LayoutInflater.from(parent.getContext()).inflate(R.layout.stories_items, parent, false);
        SearchAdapter.SearchViewHolder vh = new SearchAdapter.SearchViewHolder(v);
        return vh;
    }

    @Override
    public void onBindViewHolder(@NonNull SearchViewHolder holder, int position) {
        SetItemListInAdapter(holder,position);

        if (dataList.get(position).getVoiceLink() == null ||dataList.get(position).getVoiceLink().equals("")){
            holder.btn_voice.setVisibility(View.INVISIBLE);
            holder.btn_voice.setClickable(false);
        }
        holder.btn_voice.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                clickOnVoiceSearch.doSomeThingInVoiceSearch(dataList.get(position));
            }
        });
        holder.btn_text.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                clickOnReadSearch.doSomeThingInReadSearch(dataList.get(position));
            }
        });
        holder.layout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                clickOnSearchItem.doSomeThingInSearhitem(dataList.get(position));
            }
        });
    }

    @Override
    public int getItemCount() {
        return dataList.size();
    }

    public class SearchViewHolder extends RecyclerView.ViewHolder {

        ImageView img_book;
        Button btn_voice;
        Button btn_text;
        TextView bookTitle;
        TextView writerName;
        TextView illustrator;
        TextView input_writer;
        TextView input_illustrator;
        ConstraintLayout layout;

        public SearchViewHolder(@NonNull View v) {
            super(v);

            layout =v.findViewById(R.id.rLayout);
            img_book =  v.findViewById(R.id.img_book);
            bookTitle =  v.findViewById(R.id.bookTitle);
            writerName =  v.findViewById(R.id.writerName);
            illustrator =  v.findViewById(R.id.illustrator);
            input_writer =  v.findViewById(R.id.input_writer);
            input_illustrator =  v.findViewById(R.id.input_illustrator);
            img_book =  v.findViewById(R.id.img_book);
            btn_voice =  v.findViewById(R.id.btn_voice);
            btn_text =  v.findViewById(R.id.btn_text);
        }
    }
    void SetItemListInAdapter(SearchViewHolder holder,int position){


        holder.bookTitle.setText(String.valueOf(dataList.get(position).getName()));
        holder.input_writer.setText(dataList.get(position).getWriter());
        holder.input_illustrator.setText(dataList.get(position).getDesigner());

        String img = dataList.get(position).getImageBookLink();
        String newString = img.replace("https", "http");
        Glide.with(context).load(newString).into(holder.img_book);
    }
}
