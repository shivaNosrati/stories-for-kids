package com.example.storiesforkids.ui.MainFrg;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class CommentData {

    @SerializedName("user_name")
    @Expose
    private String userName;

    @SerializedName("comment")
    @Expose
    private String comment;

    @SerializedName("user_pic")
    @Expose
    private String user_pic;

    public String getUser_pic() {
        return user_pic;
    }

    public void setUser_pic(String user_pic) {
        this.user_pic = user_pic;
    }

    public String getUserName() {
        return userName;
    }

    public String getComment() {
        return comment;
    }

    public void setUserName(String userName) {
        this.userName = userName;
    }

    public void setComment(String comment) {
        this.comment = comment;
    }
}
