package com.example.storiesforkids.ui.LoginRegister;

import com.google.gson.annotations.SerializedName;

import java.io.File;

public class ProfileModel {

    @SerializedName("name")
    private String name;

    @SerializedName("age")
    private Integer age;

//    @SerializedName("pic")
//    private File pic;

    @SerializedName("uu_id")
    private String uu_id;

    @SerializedName("phone")
    private String phone;

    public void setName(String name) {
        this.name = name;
    }

    public void setAge(Integer age) {
        this.age = age;
    }

    public void setUu_id(String uu_id) {
        this.uu_id = uu_id;
    }

    public String getName() {
        return name;
    }

    public Integer getAge() {
        return age;
    }

    public String getUu_id() {
        return uu_id;
    }

    public String getPhone() { return phone; }

    public void setPhone(String phone) {
        this.phone = phone;
    }
}
