package com.example.storiesforkids.ui;

import androidx.fragment.app.Fragment;
import androidx.navigation.Navigation;

import com.example.storiesforkids.R;
import com.example.storiesforkids.ui.UserInformation.IbackPressListener;

import java.util.Objects;

public abstract class BaseFragment extends Fragment implements IbackPressListener {
    @Override
    public void backPress() {
        try {
            Navigation.findNavController(Objects.requireNonNull(getActivity()), R.id.nav_host_fragment).navigateUp();

        }catch (Exception e){

        }

    }
}
