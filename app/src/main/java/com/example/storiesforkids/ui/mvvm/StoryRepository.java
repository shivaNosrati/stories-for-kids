package com.example.storiesforkids.ui.mvvm;

import android.app.Application;
import androidx.lifecycle.MutableLiveData;

import com.example.storiesforkids.ui.APIClient.RetrofitInstance;
import com.example.storiesforkids.ui.Favorite.Data;
import com.example.storiesforkids.ui.MainFrg.InfoStoryModel;
import com.example.storiesforkids.ui.Service.APIService;
import com.example.storiesforkids.ui.UserInformation.UserInfo;
import com.example.storiesforkids.ui.UserInformation.UserInfoModel;
import com.google.android.exoplayer2.C;

import java.util.ArrayList;
import java.util.List;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class StoryRepository {

    private   ArrayList<Data> data = new ArrayList<>();
    private   UserInfo userData = new UserInfo();

    private Application application;
   // private String token ="eyJ0eXAiOiJKV1QiLCJhbGciOiJSUzI1NiIsImp0aSI6ImU4MDg2OWNhYjQwNjVlZmQ0YTJjMzQ4OTkwOWYwNTBhNDEyNmRlMzI0NzBmNDc3YzNhMWQ0Mjc1ZmYzNWZlYzhlOWRlY2MzZjA2MDkxYTE3In0.eyJhdWQiOiIxIiwianRpIjoiZTgwODY5Y2FiNDA2NWVmZDRhMmMzNDg5OTA5ZjA1MGE0MTI2ZGUzMjQ3MGY0NzdjM2ExZDQyNzVmZjM1ZmVjOGU5ZGVjYzNmMDYwOTFhMTciLCJpYXQiOjE1NzExMjc1NjMsIm5iZiI6MTU3MTEyNzU2MywiZXhwIjoxNjAyNzQ5OTYzLCJzdWIiOiI2Iiwic2NvcGVzIjpbXX0.f1F-3z_GOTq7a9ktGYPvMncKQFfT5ktlKosI0DujjwvT8NvrYRXS9kqdb_LTKK7fPQK-YEm-ehl3ERI0p3aAFQOuCMUx76DJ8-nLLxgZmbYfSQH2Baa0SnTyrWGkwwEb7BdPm4vE2xwKpX_JiWIqyQ8HQ_PvXEKpV_rQWyWD6Pbz7YqNwbBpVHRmuFLhTu5-vCKO6vOZTIC2GmETYSDk9rMC8L53Izm6pG0ijQel6j-DwavmCxt8YM1VgRy4txsoNWlBi0x69WIVWUAiGr8Z9UOTGkdukHvkj8kbtX-hBUVT7PZisig1B9vkZ-scP1AlJ1JMIXBQ-z97oi1Tv-lrbqqXv5344B6gIBRJfQ8hLGE6ywaBTR48VbQtVK3yhwAeMqUBZO2OhHoWS6oMJDyyjtryj3McY3XcbWVzmX_ME3SvqzS_c-DLFsl3OpOd5vysXRNnZK77hw0SO-c780_k_hQKSncvdt4cvWCH9dIlhDXsJfwhFblIFpy4BQtUj2nbjfSQYSkQxIrB1GzFciKc7faSw9o8KKSyA4U7LUeI0-Y49bdQf81PltfFTxxPk-tbEyj924nGHoZBzFXM8A2MYyw8tgKiWlARXtPJ6VWXDqUKp1UaL3LrWwyeAfMyqCFq2rIa3lc7afFdAJ76L40OKdanZ0Bhnxg5YY2eqU5eTS0" ;

    public StoryRepository(Application application) {
        this.application = application;
    }

     MutableLiveData<List<Data>> getMutableLiveData(String token) {
        final MutableLiveData<List<Data>> mutableLiveData = new MutableLiveData<>();
        APIService apiService = RetrofitInstancee.getRetrofitInstance().create(APIService.class);

        Call<InfoStoryModel> call = apiService.getDetails(token);

        call.enqueue(new Callback<InfoStoryModel>() {
            @Override
            public void onResponse(Call<InfoStoryModel> call, Response<InfoStoryModel> response) {
                InfoStoryModel storyModel = response.body();

                if (storyModel != null & storyModel.getData() != null) {
                    data = (ArrayList<Data>) storyModel.getData();
                    mutableLiveData.setValue(data);
                }
            }

            @Override
            public void onFailure(Call<InfoStoryModel> call, Throwable t) {

        //       mutableLiveData.setValue(null);
            }
        });


        return mutableLiveData;
    }

    MutableLiveData<UserInfo> getUserInfo(String token){

        final MutableLiveData<UserInfo> userLiveData = new MutableLiveData<>();
        APIService service = RetrofitInstance.getRetrofitInstance().create(APIService.class);
        Call<UserInfoModel> call = service.userInfo(token);
        call.enqueue(new Callback<UserInfoModel>() {
            @Override
            public void onResponse(Call<UserInfoModel> call, Response<UserInfoModel> response) {
                UserInfoModel storyModel2 = response.body();

                if (storyModel2 != null & storyModel2.getData() != null) {
                    userData = (UserInfo) storyModel2.getData();
                    userLiveData.setValue(userData);
                }
            }

            @Override
            public void onFailure(Call<UserInfoModel> call, Throwable t) {

           //     userLiveData.setValue(null);
            }
        });
        return userLiveData;
    }


}
