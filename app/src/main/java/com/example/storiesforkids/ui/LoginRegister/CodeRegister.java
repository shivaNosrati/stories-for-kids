package com.example.storiesforkids.ui.LoginRegister;

import android.app.ProgressDialog;
import android.content.Context;
import android.os.Build;
import android.os.Bundle;
import android.os.CountDownTimer;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.annotation.RequiresApi;
import androidx.fragment.app.Fragment;
import androidx.navigation.NavController;
import androidx.navigation.NavOptions;
import androidx.navigation.Navigation;

import com.example.storiesforkids.R;
import com.example.storiesforkids.ui.CheckNetwork.CheckNetworkConnectionUtils;
import com.example.storiesforkids.ui.CheckNetwork.INetworkCallBack;
import com.example.storiesforkids.ui.Service.NetworkService;
import com.example.storiesforkids.ui.Share.SharePreferencesHelper;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

import butterknife.BindView;
import butterknife.ButterKnife;

public class CodeRegister extends Fragment {

    @BindView(R.id.edt_enter_code)
    EditText enterCode;
    @BindView(R.id.btn_ok_code)
    Button registerButton;
    @BindView(R.id.timer)
    TextView timer;


    private Integer iCode;
    private NavController navController;
    ProgressDialog progressDialog;
    private boolean hasInternet = false;
    public static final String OTP_REGEX = "[0-9]{1,6}";


    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {

        View view = inflater.inflate(R.layout.activity_code_register, container, false);
        navController = Navigation.findNavController(getActivity(), R.id.nav_login_host);
        ((BaseLoginActivity) getActivity()).updateStatusBarColor("#CD0202");


        CheckNetworkConnectionUtils.getInstance().checkHasInternet(getContext(), new INetworkCallBack() {

            @Override
            public void hasInternet() {
                hasInternet = true;

            }

            @Override
            public void noInternet() {
                Toast.makeText(getContext(), "اینترنت خود را چک کنید", Toast.LENGTH_SHORT).show();
                hasInternet = true;

            }
        });
        ButterKnife.bind(this, view);
        return view;
    }


    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        initTimer();

        registerButton.setOnClickListener(new View.OnClickListener() {
            @RequiresApi(api = Build.VERSION_CODES.M)
            @Override
            public void onClick(View v) {

                if (hasInternet) {
                    Register();
                } else
                    hasInternet = true;


            }
        });
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
    }

    private void Register() {
        if (!validate()) {
            return;
        }
        registerButton.setClickable(false);
        progressDialog = new ProgressDialog(getContext(), R.style.AppTheme_Green_Dialog);
        progressDialog.setIndeterminate(true);
        progressDialog.setMessage("صبور باشید... ");
        progressDialog.show();
        progressDialog.setCancelable(false);

        String phoneNumber = getArguments().getString("phone");
        String uuId = getArguments().getString("uuid");

        if (phoneNumber != null && uuId != null) {

            iCode = Integer.valueOf(enterCode.getText().toString());
            sendRequestRegister(phoneNumber, uuId, iCode);
        }

    }

    private void initTimer() {
        final int[] time = {60};

        new CountDownTimer(60000, 1000) {

            public void onTick(long millisUntilFinished) {
                timer.setText("0:" + checkDigit(time[0]));
                time[0]--;
                timer.setClickable(false);
                timer.setGravity(0);
            }

            public void onFinish() {
                timer.setText("ارسال مجدد");
                timer.setClickable(true);
                timer.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {

                        String phoneNumber = getArguments().getString("phone");
                        String uuId = getArguments().getString("uuid");
                        timer.setVisibility(View.INVISIBLE);

                        registerButton.setClickable(false);
                        progressDialog = new ProgressDialog(getContext(), R.style.AppTheme_Green_Dialog);
                        progressDialog.setIndeterminate(true);
                        progressDialog.setMessage("صبور باشید... ");
                        progressDialog.show();
                        progressDialog.setCancelable(false);

                        sendRequestToSeLogin(phoneNumber, uuId);
                    }
                });
            }

        }.start();

    }

    private void sendRequestToSeLogin(String phoneNumber, String uuId) {

        NetworkService.getService().registerWithPhone(phoneNumber, uuId, new RegisterCallBack() {
            @Override
            public void doRegister() {
                progressDialog.dismiss();
                registerButton.setClickable(true);

                Bundle dataBundle = new Bundle();
                dataBundle.putString("telephone", phoneNumber);


            }

            @Override
            public void notRegister(Throwable t) {

                progressDialog.dismiss();
                registerButton.setClickable(true);

            }

            @Override
            public void dublicateNumber() {
                registerButton.setClickable(true);
                progressDialog.dismiss();
                Toast.makeText(getContext(), "خطا در بررسی", Toast.LENGTH_SHORT).show();
            }
        });
    }

    private String checkDigit(int number) {

        return number <= 9 ? "0" + number : String.valueOf(number);
    }

    private void sendRequestRegister(String phone, String uuid, Integer code) {

        NetworkService.getService().registerWithCode(phone, uuid, code, new VerifyCallBack() {
            @Override
            public void onReceiveToken( String token) {
                progressDialog.dismiss();
                storeToken("Bearer " + token);

                Bundle bundle = new Bundle();

                bundle.putString("phoneNumber", phone);
                bundle.putString("uuidUser", uuid);

//                navController.navigate(R.id.setProfileFragment , bundle);
                registerButton.setClickable(true);
                navController.navigate(R.id.setProfileFragment, bundle, new NavOptions.Builder().setPopUpTo(R.id.phoneRegister1, true).build());
            }

            @Override
            public void notReceive() {
                registerButton.setClickable(true);
                progressDialog.dismiss();
                Toast.makeText(getContext(), "اینترنت خود راچک کنید", Toast.LENGTH_SHORT).show();
            }

            @Override
            public void onDublicate() {
                registerButton.setClickable(true);
                progressDialog.dismiss();
                Toast.makeText(getContext(), "کد وارد شده صحیح نیست", Toast.LENGTH_SHORT).show();
            }
        });


    }

    private void storeToken(String token) {

        SharePreferencesHelper.getInatance().insertString(getContext(), token);
    }

    public boolean validate() {
        boolean valid = true;

        String code = enterCode.getText().toString();

        if (code.isEmpty()) {
            enterCode.setError("کد وارد نمایید");
            registerButton.setClickable(true);
            valid = false;
        } else if (code.length() != 5) {

            registerButton.setClickable(true);
            enterCode.setError("کد پنج رقمی است");
            valid = false;

        } else
            enterCode.setError(null);

        return valid;
    }
}
