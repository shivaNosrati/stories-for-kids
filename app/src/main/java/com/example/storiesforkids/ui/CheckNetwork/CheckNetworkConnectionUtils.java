package com.example.storiesforkids.ui.CheckNetwork;

import android.content.Context;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;

public class CheckNetworkConnectionUtils implements IcheckNetworkConnectionUtils {

    private static CheckNetworkConnectionUtils mCheckNetworkConnectionUtils = null;

    private CheckNetworkConnectionUtils() {
    }

    public static CheckNetworkConnectionUtils getInstance() {

        if (mCheckNetworkConnectionUtils == null) {

            mCheckNetworkConnectionUtils = new CheckNetworkConnectionUtils();

            return mCheckNetworkConnectionUtils;
        }

        return mCheckNetworkConnectionUtils;
    }

    @Override
    public boolean isNetworkConnected(Context context) {
        ConnectivityManager cm = (ConnectivityManager) context.getSystemService(Context.CONNECTIVITY_SERVICE);

        NetworkInfo ni = cm.getActiveNetworkInfo();

        if (ni == null) {
            return false;

        } else {
            return true;

        }
    }

    @Override
    public void checkHasInternet(Context context, INetworkCallBack callBack) {
        boolean hasInternet = isNetworkConnected(context);


        if (hasInternet) {

            callBack.hasInternet();

        } else {
            callBack.noInternet();

        }

    }

}

