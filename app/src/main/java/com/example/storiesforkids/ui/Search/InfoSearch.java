package com.example.storiesforkids.ui.Search;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.List;

public class InfoSearch {

    @SerializedName("id")
    @Expose
    private Integer id;

    @SerializedName("category_id")
    @Expose
    private Integer categoryId;

    @SerializedName("name")
    @Expose
    private String name;

    @SerializedName("title")
    @Expose
    private String title;

    @SerializedName("writer")
    @Expose
    private String writer;

    @SerializedName("publisher")
    @Expose
    private String publisher;

    @SerializedName("designer")
    @Expose
    private String designer;

    @SerializedName("talker")
    @Expose
    private String talker;

    @SerializedName("abstract")
    @Expose
    private String _abstract;

    @SerializedName("age")
    @Expose
    private String age;

    @SerializedName("view_count")
    @Expose
    private Integer viewCount;

    @SerializedName("download_count")
    @Expose
    private Integer downloadCount;

    @SerializedName("voice_name")
    @Expose
    private String voiceName;

    @SerializedName("book_image")
    @Expose
    private String bookImage;

    @SerializedName("created_at")
    @Expose
    private String createdAt;

    @SerializedName("updated_at")
    @Expose
    private String updatedAt;

    @SerializedName("section_body")
    @Expose
    private String sectionBody;

    @SerializedName("is_like")
    @Expose
    private Boolean isLike;

    @SerializedName("comment")
    @Expose
    private List<Object> comment;

    @SerializedName("voice_link")
    @Expose
    private String voiceLink;

    @SerializedName("voice_size")
    @Expose
    private Integer voiceSize;

    @SerializedName("story_paragraph")
    @Expose
    private List<StoryParagraph> storyParagraph;

    @SerializedName("favorite_count")
    @Expose
    private Integer favoriteCount;

    @SerializedName("image_book_link")
    @Expose
    private String imageBookLink;


    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public Integer getCategoryId() {
        return categoryId;
    }

    public void setCategoryId(Integer categoryId) {
        this.categoryId = categoryId;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getWriter() {
        return writer;
    }

    public void setWriter(String writer) {
        this.writer = writer;
    }

    public String getPublisher() {
        return publisher;
    }

    public void setPublisher(String publisher) {
        this.publisher = publisher;
    }

    public String getDesigner() {
        return designer;
    }

    public void setDesigner(String designer) {
        this.designer = designer;
    }

    public String getTalker() {
        return talker;
    }

    public void setTalker(String talker) {
        this.talker = talker;
    }

    public String getAbstract() {
        return _abstract;
    }

    public void setAbstract(String _abstract) {
        this._abstract = _abstract;
    }

    public String getAge() {
        return age;
    }

    public void setAge(String age) {
        this.age = age;
    }

    public Integer getViewCount() {
        return viewCount;
    }

    public void setViewCount(Integer viewCount) {
        this.viewCount = viewCount;
    }

    public Integer getDownloadCount() {
        return downloadCount;
    }

    public void setDownloadCount(Integer downloadCount) {
        this.downloadCount = downloadCount;
    }

    public String getVoiceName() {
        return voiceName;
    }

    public void setVoiceName(String voiceName) {
        this.voiceName = voiceName;
    }

    public String getBookImage() {
        return bookImage;
    }

    public void setBookImage(String bookImage) {
        this.bookImage = bookImage;
    }

    public String getCreatedAt() {
        return createdAt;
    }

    public void setCreatedAt(String createdAt) {
        this.createdAt = createdAt;
    }

    public String getUpdatedAt() {
        return updatedAt;
    }

    public void setUpdatedAt(String updatedAt) {
        this.updatedAt = updatedAt;
    }

    public String getSectionBody() {
        return sectionBody;
    }

    public void setSectionBody(String sectionBody) {
        this.sectionBody = sectionBody;
    }

    public Boolean getIsLike() {
        return isLike;
    }

    public void setIsLike(Boolean isLike) {
        this.isLike = isLike;
    }

    public List<Object> getComment() {
        return comment;
    }

    public void setComment(List<Object> comment) {
        this.comment = comment;
    }

    public String getVoiceLink() {
        return voiceLink;
    }

    public void setVoiceLink(String voiceLink) {
        this.voiceLink = voiceLink;
    }

    public Integer getVoiceSize() {
        return voiceSize;
    }

    public void setVoiceSize(Integer voiceSize) {
        this.voiceSize = voiceSize;
    }

    public List<StoryParagraph> getStoryParagraph() {
        return storyParagraph;
    }

    public void setStoryParagraph(List<StoryParagraph> storyParagraph) {
        this.storyParagraph = storyParagraph;
    }

    public Integer getFavoriteCount() {
        return favoriteCount;
    }

    public void setFavoriteCount(Integer favoriteCount) {
        this.favoriteCount = favoriteCount;
    }

    public String getImageBookLink() {
        return imageBookLink;
    }

    public void setImageBookLink(String imageBookLink) {
        this.imageBookLink = imageBookLink;
    }
}
