package com.example.storiesforkids.ui.Search;

import com.example.storiesforkids.ui.MainFrg.InfoStoryModel;


public interface ResponsesSearch {
    void  onResponse (InfoSearchModel  response);
    void onFailure (Throwable throwable);
}
