package com.example.storiesforkids.ui.AllOfStoryInfo;

public interface SetFaveCallback {

    void storyIsFave();
    void storyNotSetToFave(Throwable t);
}
