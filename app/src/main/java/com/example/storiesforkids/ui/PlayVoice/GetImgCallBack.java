package com.example.storiesforkids.ui.PlayVoice;

import com.example.storiesforkids.ui.Favorite.Data;

public interface GetImgCallBack {

    void onReceived(Data storyParagraphList);

    void notReceived(Throwable t);
}
