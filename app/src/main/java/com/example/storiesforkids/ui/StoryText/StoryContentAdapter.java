package com.example.storiesforkids.ui.StoryText;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.example.storiesforkids.R;
import com.example.storiesforkids.ui.PlayVoice.ParagraphData;

import java.util.List;

public class StoryContentAdapter extends RecyclerView.Adapter<StoryContentAdapter.ViewHolder> {

    private List<ParagraphData> dataList;
    private Context context;
//    private ClickOnFaveImg clickOnFaveImg;


    public StoryContentAdapter(List<ParagraphData> dataList , Context context ) {

        this.dataList=dataList;
        this.context =context;

    }

    @NonNull
    @Override
    public StoryContentAdapter.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View v = LayoutInflater.from(parent.getContext()).inflate(R.layout.story_content_item, parent, false);
        StoryContentAdapter.ViewHolder vh = new StoryContentAdapter.ViewHolder(v);
        return vh;
    }

    @Override
    public void onBindViewHolder(@NonNull ViewHolder holder, int position) {

        holder.txt_book.setText(dataList.get(position).getContent());

    }



    @Override
    public int getItemCount() {
        return dataList.size();
    }


    public class ViewHolder extends RecyclerView.ViewHolder {

        private TextView txt_book;


        public ViewHolder(View v) {
            super(v);

            txt_book =  v.findViewById(R.id.txt_content);

        }
    }
}
