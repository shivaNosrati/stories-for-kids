package com.example.storiesforkids.ui.PlayVoice;


import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class ParagraphData {

    @SerializedName("id")
    @Expose
    private Integer id;

    @SerializedName("story_id")
    @Expose
    private Integer storyId;


    @SerializedName("number")
    @Expose
    private Integer number;


    @SerializedName("pic_link")
    @Expose
    private String pic;


    @SerializedName("pic_time")
    @Expose
    private String picTime;


    @SerializedName("content")
    @Expose
    private String content;


    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public Integer getStoryId() {
        return storyId;
    }

    public void setStoryId(Integer storyId) {
        this.storyId = storyId;
    }

    public Integer getNumber() {
        return number;
    }

    public void setNumber(Integer number) {
        this.number = number;
    }

    public String getPic() {
        return pic;
    }

    public void setPic(String pic) {
        this.pic = pic;
    }

    public String getPicTime() {
        return picTime;
    }

    public void setPicTime(String picTime) {
        this.picTime = picTime;
    }

    public String getContent() {
        return content;
    }

    public void setContent(String content) {
        this.content = content;
    }


}
