package com.example.storiesforkids.ui.Search;

import android.content.Context;
import android.content.pm.ActivityInfo;
import android.os.Build;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.annotation.RequiresApi;
import androidx.appcompat.widget.SearchView;
import androidx.fragment.app.Fragment;
import androidx.navigation.NavController;
import androidx.navigation.Navigation;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.example.storiesforkids.R;
import com.example.storiesforkids.ui.History.SetHistoryCallback;
import com.example.storiesforkids.ui.App;
import com.example.storiesforkids.ui.MainView.MainActivity;
import com.example.storiesforkids.ui.Service.NetworkService;
import com.example.storiesforkids.ui.Share.SharePreferencesHelper;


import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;

public class SearchFragment extends Fragment implements ClickOnSearchItem, ClickOnReadSearch, ClickOnVoiceSearch {

    @BindView(R.id.search_view)
    SearchView searchView;

    @BindView(R.id.recycler_search)
    RecyclerView rc_Search;

    private SearchAdapter searchAdapter;
    private DeleteSomeWidgetForSearch viewListener;
    private NavController navController;


    @RequiresApi(api = Build.VERSION_CODES.LOLLIPOP)
    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view =inflater.inflate(R.layout.search_frg , container , false);
        ButterKnife.bind(this, view);
        navController = Navigation.findNavController(getActivity(), R.id.nav_host_fragment);
        ((MainActivity)getActivity()).updateStatusBarColor("#19AB63");
        getActivity().setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_PORTRAIT);
        viewListener.hideSomWidget();

        return view ;

    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        viewListener.hideSomWidget();
        searchView.setIconified(false);
        searchView.setOnQueryTextListener(new SearchView.OnQueryTextListener() {

            @Override
            public boolean onQueryTextSubmit(String query) {
                // collapse the view ?
                //menu.findItem(R.id.menu_search).collapseActionView();

                return false;
            }

            @Override
            public boolean onQueryTextChange(String newText) {
                sendDatatoSever(newText);
                Log.d("searchRc","text  :  "+newText);
                return false;
            }
        });
        }




    void sendDatatoSever (String strSearch){
        NetworkService.getService().getsearchresponse(strSearch, getToken(), new ResponsesSearch() {
            @Override
            public void onResponse(InfoSearchModel response) {

                fillSearchList(response.getData());

            }

            @Override
            public void onFailure(Throwable throwable) {
                Log.d("searchRc","text  : fiald" + throwable);
            }
        });
    }

    private void fillSearchList(List<InfoSearch> data) {
        searchAdapter=new SearchAdapter(data, App.context,this);
        RecyclerView.LayoutManager layoutManager = new LinearLayoutManager(getActivity());
        rc_Search.setLayoutManager(layoutManager);
        rc_Search.setAdapter(searchAdapter);
        searchAdapter.notifyDataSetChanged();

    }

    String getToken (){
        return SharePreferencesHelper.getInatance().getStringValueFromShare(App.context);
    }

    @Override
    public void onAttach(@NonNull Context context) {
        super.onAttach(context);

        if (context instanceof DeleteSomeWidgetForSearch){
            this.viewListener = (DeleteSomeWidgetForSearch) context;
        }
    }


    @Override
    public void onDetach() {
        super.onDetach();
        viewListener=null;
    }

    @Override
    public void onPause() {
        super.onPause();

        if (android.os.Build.VERSION.SDK_INT >= 27) {
            getActivity().setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_PORTRAIT);
        }
    }

    @Override
    public void onResume() {
        super.onResume();

        ((MainActivity) getActivity()).goneUi();
        getActivity().setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_PORTRAIT);
    }

    @Override
    public void doSomeThingInSearhitem(InfoSearch infoSearch) {

        Bundle dataBundle = new Bundle();
        dataBundle.putInt("id" , infoSearch.getId());
        dataBundle.putString("designer" ,infoSearch.getDesigner());
        dataBundle.putString("storyName" ,infoSearch.getName());
        dataBundle.putString("Talker" ,infoSearch.getTalker());
        dataBundle.putString("storyTitle" ,infoSearch.getTitle());
        dataBundle.putString("writer" ,infoSearch.getWriter());
        dataBundle.putString("img" ,infoSearch.getImageBookLink());
        dataBundle.putString("voice" , infoSearch.getVoiceLink());
        dataBundle.putBoolean("isLiked" , infoSearch.getIsLike());

        navController.navigate(R.id.detailsStoryFragment2 , dataBundle);
    }

    @Override
    public void doSomeThingInReadSearch(InfoSearch infoSearch) {

        Bundle bundle = new Bundle();
        bundle.putString("storyName" ,infoSearch.getName());
        bundle.putInt("id" , infoSearch.getId());

        String token = SharePreferencesHelper.getInatance().getStringValueFromShare(getContext());
        NetworkService.getService().setHis(infoSearch.getId(), token, new SetHistoryCallback() {
            @Override
            public void historyIsSet() {

            }

            @Override
            public void historyNotSet(Throwable t) {
                Toast.makeText(getContext(), ""+t.getMessage(), Toast.LENGTH_SHORT).show();
            }
        });

        navController.navigate(R.id.storyContentFragment , bundle);

    }

    @Override
    public void doSomeThingInVoiceSearch(InfoSearch infoSearch) {
        Bundle bundle = new Bundle();
        bundle.putString("voice_link" , infoSearch.getVoiceLink());
        bundle.putInt("sId" , infoSearch.getId());

        navController.navigate(R.id.playVoiceFragment2 , bundle);

    }


    public interface DeleteSomeWidgetForSearch{
        void hideSomWidget();
    }
}
