package com.example.storiesforkids.ui.LoginRegister;

public interface LogOutCallBack {

    void userIsLogout();

    void userNotLogout(Throwable t);
}
