package com.example.storiesforkids.ui.AllOfStoryInfo;

import android.content.Context;
import android.content.pm.ActivityInfo;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.IntRange;
import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.annotation.RequiresApi;
import androidx.core.app.ActivityCompat;
import androidx.core.view.ViewCompat;
import androidx.fragment.app.Fragment;
import androidx.navigation.NavController;
import androidx.navigation.Navigation;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import androidx.transition.Transition;
import androidx.transition.TransitionInflater;

import com.bumptech.glide.Glide;
import com.downloader.PRDownloader;
import com.example.storiesforkids.R;
import com.example.storiesforkids.ui.BaseFragment;
import com.example.storiesforkids.ui.Comments.Comment;
import com.example.storiesforkids.ui.Comments.CommentAdapter;
import com.example.storiesforkids.ui.Comments.CommentModel;
import com.example.storiesforkids.ui.Comments.GetCommentsCallback;
import com.example.storiesforkids.ui.Favorite.Data;
import com.example.storiesforkids.ui.History.SetHistoryCallback;
import com.example.storiesforkids.ui.MainFrg.InfoStoryModel;
import com.example.storiesforkids.ui.MainView.MainActivity;
import com.example.storiesforkids.ui.MainView.SomeChangeInView;
import com.example.storiesforkids.ui.Service.NetworkService;
import com.example.storiesforkids.ui.Share.SharePreferencesHelper;
import com.example.storiesforkids.ui.Simolor.ClickOnSimImg;
import com.example.storiesforkids.ui.Simolor.GetSimilorsCallback;
import com.example.storiesforkids.ui.Simolor.SimilorAdapter;

import java.util.List;
import java.util.Objects;

import butterknife.BindView;
import butterknife.ButterKnife;


public class DetailsStoryFragment extends BaseFragment implements ActivityCompat.OnRequestPermissionsResultCallback, ClickOnSimImg {

    @BindView(R.id.storyPic)
    ImageView imgBook;

    @BindView(R.id.sName)
    TextView name;

    @BindView(R.id.bback)
    ImageView back;

    @BindView(R.id.img_like)
    ImageView like;

    @BindView(R.id.icon_read)
    ImageView readStory;

    @BindView(R.id.img_play_in_details)
    ImageView btn_play;

    @BindView(R.id.nevisande)
    TextView wrter;

    @BindView(R.id.tarah)
    TextView designer;

    @BindView(R.id.sim_recycler)
    RecyclerView recyclerView;

    @BindView(R.id.lastCommentRecycler)
    RecyclerView cRecycler;


    @BindView(R.id.progressBarOne)
    ProgressBar progressBarOne;

    @BindView(R.id.inputCommentIcon)
    ImageView inputYourComment;

    private SimilorAdapter adapter;
    private NavController navController;
//    private static String file_url = "";
    private SomeChangeInView goneSomeThing;

    private Integer id;
    private String token;
    private String nameS;
    private String img;
    private String n;
    private String t;
    private String voiceUri;
    private static Boolean isLiked;
    private static final String ARG_KITTEN_NUMBER = "argKittenNumber";
    String bookimg;

    public static DetailsStoryFragment newInstance(int kittenNumber) {
        Bundle args = new Bundle();
        args.putInt(ARG_KITTEN_NUMBER, kittenNumber);

        DetailsStoryFragment fragment = new DetailsStoryFragment();
        fragment.setArguments(args);

        return fragment;
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);

        bookimg = String.valueOf(getArguments().getString("img"));
        img = bookimg.replace("https", "http");
        voiceUri = Objects.requireNonNull(getArguments()).getString("voice_link");
        id = Integer.valueOf(Objects.requireNonNull(getArguments().get("id")).toString());
        token = SharePreferencesHelper.getInatance().getStringValueFromShare(getContext());
        nameS = Objects.requireNonNull(getArguments().get("storyName")).toString();
     // img = Objects.requireNonNull(getArguments().get("img")).toString();

        n = Objects.requireNonNull(getArguments().get("writer")).toString();
        t = Objects.requireNonNull(getArguments().get("designer")).toString();
      //  file_url = Objects.requireNonNull(getArguments().get("voice_link")).toString();
        isLiked = getArguments().getBoolean("isLiked");

        if(context instanceof SomeChangeInView)
            this.goneSomeThing= (SomeChangeInView) context;

    }

    @RequiresApi(api = Build.VERSION_CODES.LOLLIPOP)
    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setHasOptionsMenu(true);

        setSharedElementReturnTransition(TransitionInflater.from(getActivity()).inflateTransition(android.R.transition.fade));

        setExitTransition(TransitionInflater.from(getActivity()).inflateTransition(android.R.transition.slide_left));

        setSharedElementEnterTransition(TransitionInflater.from(getActivity()).inflateTransition(android.R.transition.slide_right));

    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {

        View view = inflater.inflate(R.layout.like_and_download, container, false);
        ButterKnife.bind(this, view);
        ((MainActivity) Objects.requireNonNull(getActivity())).updateStatusBarColor("#6B0BB5");
        navController = Navigation.findNavController(getActivity(), R.id.nav_host_fragment);
//        PRDownloader.initialize(getContext());
//        ((MainActivity) getActivity()).goneUi();
        return view;
    }

    @RequiresApi(api = Build.VERSION_CODES.LOLLIPOP)
    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        setData();


    }


    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);

        goneSomeThing.onFragmentChangeListener(this);
        //setData();
//        getDetails();
        initBack();
        initLike();
        initSimilar();
        sendMyComment();
        initPlay();
        accessToStoryText();
        RequestToGetComment();
        checkLike();
        checkVoiceIsAvailable();

    }


    @Override
    public void onPause() {
        super.onPause();

        if (android.os.Build.VERSION.SDK_INT >= 27) {
            getActivity().setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_PORTRAIT);
        }
    }

    @Override
    public void onResume() {
        super.onResume();
        getActivity().setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_PORTRAIT);
        ((MainActivity) getActivity()).goneUi();


    }


    private void getDetails() {

        voiceUri = Objects.requireNonNull(getArguments()).getString("voice");
        id = Integer.valueOf(Objects.requireNonNull(getArguments().get("id")).toString());
        token = SharePreferencesHelper.getInatance().getStringValueFromShare(getContext());
        nameS = Objects.requireNonNull(getArguments().get("storyName")).toString();
        img = Objects.requireNonNull(getArguments().get("img")).toString();
        n = Objects.requireNonNull(getArguments().get("writer")).toString();
        t = Objects.requireNonNull(getArguments().get("designer")).toString();
//        file_url = Objects.requireNonNull(getArguments().get("voice_link")).toString();
        isLiked = getArguments().getBoolean("isLiked");
    }

    private void sendMyComment() {
        inputYourComment.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                Bundle bundle = new Bundle();
                bundle.putInt("id" ,id);
                bundle.putString("voice" ,voiceUri);
                bundle.putString("storyName" , nameS);
                bundle.putString("img" ,img);
                bundle.putString("writer" ,n);
                bundle.putString("designer" ,t);
                bundle.putBoolean("isLiked" ,isLiked);

                navController.navigate(R.id.commentFragment , bundle);
            }
        });
    }

    private void accessToStoryText() {

        readStory.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                insertInHistory();
                Bundle bundle = new Bundle();
                bundle.putInt("id" ,id);
                bundle.putString("storyName" , nameS);
                bundle.putString("img" , img);
                navController.navigate(R.id.storyContext2 ,bundle);

            }
        });
    }

    private void initPlay() {
        btn_play.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Bundle b = new Bundle();
                b.putString("voice_link", voiceUri);
                b.putInt("sId" , id);

                navController.navigate(R.id.playVoiceFragment , b);

            }
        });
    }

    private void RequestToGetComment() {

        NetworkService.getService().userComment(id, token, new GetCommentsCallback() {
            @Override
            public void onReceive(CommentModel commentModel) {
                fillCommentsList(commentModel.getData());

            }

            @Override
            public void notReceive(Throwable t) {
                Log.e("2729", ""+t.getMessage());

            }
        });
    }

    private void fillCommentsList(List<Comment> data) {
        CommentAdapter cAdapter = new CommentAdapter(data, getContext(), (MainActivity) getActivity());
        RecyclerView.LayoutManager layoutManager = new LinearLayoutManager(getActivity());
        cRecycler.setLayoutManager(layoutManager);
        cRecycler.setAdapter(cAdapter);
    }

    private void initSimilar() {

        NetworkService.getService().getSim(id,token, new GetSimilorsCallback() {
            @Override
            public void onReceived(InfoStoryModel similorModel) {
                fillSimList(similorModel.getData());

            }

            @Override
            public void notReceived(Throwable t) {
    //            Toast.makeText(getContext(), ""+t.getMessage(), Toast.LENGTH_SHORT).show();
            }
        });
    }


    private void fillSimList(List<Data> data) {

        adapter = new SimilorAdapter(data , getContext() ,this);
        LinearLayoutManager layoutManager = new LinearLayoutManager(getContext(), LinearLayoutManager.HORIZONTAL, false);
        recyclerView.setLayoutManager(layoutManager);
        recyclerView.setAdapter(adapter);
    }

    private void checkLike() {
        if (isLiked){

            like.setImageDrawable(getResources().getDrawable(R.drawable.like2));

        }
        else
            like.setImageDrawable(getResources().getDrawable(R.drawable.heart));

    }

    private void initLike() {
        like.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                if (isLiked){
                    like.setImageDrawable(getResources().getDrawable(R.drawable.heart));
                    sendRequestForDislike(id , token);

                }else
                    like.setImageDrawable(getResources().getDrawable(R.drawable.like2));
                    sendRequestLikeInteger(id ,token);

            }
        });
    }

    private void sendRequestForDislike(Integer id, String token) {
        NetworkService.getService().disLike(id, token, new DisLikeCallback() {
            @Override
            public void storyIsDisLiked() {
                Log.e("dislike" ,"لایک نیست  ");
                isLiked=false;
            }

            @Override
            public void Error() {
//                Toast.makeText(getContext() , "خطا در اتصال به شبکه" , Toast.LENGTH_SHORT).show();
            }

            @Override
            public void storyIsNotDisLiked(Throwable t) {
//                Toast.makeText(getContext() , "خطا در اتصال به شبکه" , Toast.LENGTH_SHORT).show();
            }
        });
    }

    private void sendRequestLikeInteger(Integer id, String token) {

        NetworkService.getService().setFav(id, token, new SetFaveCallback() {

            @Override
            public void storyIsFave() {


                like.setClickable(true);
                isLiked = true;
                Log.e("likee" ,"لایک شد  ");
            }

            @Override
            public void storyNotSetToFave(Throwable t) {

                Toast.makeText(getContext(), ""+t.getMessage(), Toast.LENGTH_SHORT).show();

            }
        });
    }

    private void setData() {

        wrter.setText(n);
        designer.setText(t);
        name.setText(nameS);

            imgBook.setImageURI(Uri.parse(img));
        imgBook.setScaleType(ImageView.ScaleType.FIT_XY);
        Glide.with(Objects.requireNonNull(getContext()))
                .load(img)
                .into(imgBook);


    }

    private void initBack() {
        back.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                navController.navigate(R.id.mainFragment);
            }
        });
    }


    @Override
    public void goToDetailsClickedStory(Data infoStory) {

        Bundle dataBundle = new Bundle();
        dataBundle.putInt("id" , infoStory.getId());
        dataBundle.putString("designer" ,infoStory.getDesigner());
        dataBundle.putString("storyName" ,infoStory.getName());
        dataBundle.putString("Talker" ,infoStory.getTalker());
        dataBundle.putString("storyTitle" ,infoStory.getTitle());
        dataBundle.putString("writer" ,infoStory.getWriter());
        dataBundle.putString("img" ,infoStory.getImageBookLink());
        dataBundle.putString("voice" , infoStory.getVoiceLink());
        dataBundle.putBoolean("isLiked" , infoStory.getIs_like());

        navController.navigate(R.id.detailsStoryFragment , dataBundle);

    }

    private void checkVoiceIsAvailable() {

        if (voiceUri == null ||voiceUri.equals("")){
            btn_play.setVisibility(View.GONE);
            btn_play.setClickable(false);
        }
    }

    private void insertInHistory() {
        String token = SharePreferencesHelper.getInatance().getStringValueFromShare(getContext());
        NetworkService.getService().setHis(id, token, new SetHistoryCallback() {
            @Override
            public void historyIsSet() {

            }

            @Override
            public void historyNotSet(Throwable t) {
                Toast.makeText(getContext(), ""+t.getMessage(), Toast.LENGTH_SHORT).show();
            }
        });


    }

}
