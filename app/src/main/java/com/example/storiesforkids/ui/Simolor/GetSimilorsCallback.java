package com.example.storiesforkids.ui.Simolor;

import com.example.storiesforkids.ui.Favorite.InfoFaveMogel;
import com.example.storiesforkids.ui.MainFrg.InfoStoryModel;

public interface GetSimilorsCallback {

    void onReceived(InfoStoryModel similorModel);

    void notReceived(Throwable t);
}
