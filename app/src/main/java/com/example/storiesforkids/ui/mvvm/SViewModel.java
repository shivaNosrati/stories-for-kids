package com.example.storiesforkids.ui.mvvm;

import android.app.Application;

import androidx.annotation.NonNull;
import androidx.lifecycle.AndroidViewModel;
import androidx.lifecycle.LiveData;
import androidx.lifecycle.MutableLiveData;
import androidx.lifecycle.Transformations;

import com.example.storiesforkids.ui.Favorite.Data;
import com.example.storiesforkids.ui.Share.SharePreferencesHelper;
import com.example.storiesforkids.ui.UserInformation.UserInfo;
import com.example.storiesforkids.ui.UserInformation.UserInfoWithoutPhone;

import java.util.List;

public class SViewModel extends AndroidViewModel {

    public static StoryRepository storyRepository;
    public static MutableLiveData<List<Data>> mutableLiveData;
    public static MutableLiveData<UserInfo> userleLiveData;

    private String token = SharePreferencesHelper.getInatance().getStringValueFromShare(getApplication());

    public SViewModel(@NonNull Application application) {
        super(application);
        storyRepository = new StoryRepository(application);
        mutableLiveData = storyRepository.getMutableLiveData(token);
        userleLiveData = storyRepository.getUserInfo(token);
    }

    public LiveData<List<Data>> getAllStories() {
        return mutableLiveData;
    }


    public LiveData<UserInfo> getInfo(){ return userleLiveData;}

    MutableLiveData<UserInfoWithoutPhone> userData = new MutableLiveData<>();
    public LiveData userLiveData = Transformations.switchMap(userData, id ->
            storyRepository.getUserInfo(token)); // Returns LiveData

}
