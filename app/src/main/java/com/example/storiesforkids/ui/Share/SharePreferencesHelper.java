package com.example.storiesforkids.ui.Share;

import android.content.Context;
import android.content.SharedPreferences;

import static android.content.Context.MODE_PRIVATE;

public class SharePreferencesHelper {

    private static SharePreferencesHelper mSharePreferencesHelper =null;
    private static final String SHARE = "share";
    private static final String TOKEN = "token";
    private static final String UUID = "uuid";
    private static final String AGE = "age";
    private static final String EDIT = "edit";
    private static final String Evalue = "value";
    private static final String PValue = "pValue";
    private static final String NAME = "name";
    private static final String PHONE = "phone";
    private static final String PIC = "pic";


    private SharePreferencesHelper(){ }
    public static SharePreferencesHelper getInatance(){

        if (mSharePreferencesHelper == null){
            mSharePreferencesHelper = new SharePreferencesHelper();
            return mSharePreferencesHelper;
        }
        return mSharePreferencesHelper;
    }
    public void insertString(Context context, String st){

        SharedPreferences.Editor editor = context.getSharedPreferences(SHARE, MODE_PRIVATE).edit();
        editor.putString(TOKEN,st);
        editor.apply();
    }

    public void insertEditValue(Context context, Integer value){

        SharedPreferences.Editor editor = context.getSharedPreferences(EDIT, MODE_PRIVATE).edit();
        editor.putInt(Evalue,value);
        editor.apply();
    }

    public void insertEditValueinProfile(Context context, Integer value){

        SharedPreferences.Editor editor = context.getSharedPreferences(EDIT, MODE_PRIVATE).edit();
        editor.putInt(PValue,value);
        editor.apply();
    }

    public void insertNameValueinProfile(Context context, String value){

        SharedPreferences.Editor editor = context.getSharedPreferences(EDIT, MODE_PRIVATE).edit();
        editor.putString(NAME,value);
        editor.apply();
    }

    public void insertAgeValueinProfile(Context context, Integer value){

        SharedPreferences.Editor editor = context.getSharedPreferences(EDIT, MODE_PRIVATE).edit();
        editor.putInt(AGE,value);
        editor.apply();
    }

    public void insertPhoneValueinProfile(Context context, String value){

        SharedPreferences.Editor editor = context.getSharedPreferences(EDIT, MODE_PRIVATE).edit();
        editor.putString(PHONE,value);
        editor.apply();
    }

    public void insertPicValueinProfile(Context context, String value){

        SharedPreferences.Editor editor = context.getSharedPreferences(EDIT, MODE_PRIVATE).edit();
        editor.putString(PIC,value);
        editor.apply();
    }


    public void insertUuidtString(Context context, String uuid){

        SharedPreferences.Editor editor = context.getSharedPreferences(SHARE, MODE_PRIVATE).edit();
        editor.putString(UUID,uuid);
        editor.apply();
    }


    public String
    getStringValueFromShare(Context context){

        SharedPreferences prefs = context.getSharedPreferences(SHARE, MODE_PRIVATE);
        return prefs.getString(TOKEN, null);

    }

    public Integer getIntegerValueFromEdit(Context context){

        SharedPreferences prefs = context.getSharedPreferences(EDIT, MODE_PRIVATE);
        return prefs.getInt(Evalue, 0);

    }

    public Integer getIntegerValueFromEditProfile(Context context){

        SharedPreferences prefs = context.getSharedPreferences(EDIT, MODE_PRIVATE);
        return prefs.getInt(PValue, 0);

    }

    public String getNameValueFromEditProfile(Context context){

        SharedPreferences prefs = context.getSharedPreferences(EDIT, MODE_PRIVATE);
        return prefs.getString(NAME, null);

    }

    public Integer getAgeValueFromEditProfile(Context context){

        SharedPreferences prefs = context.getSharedPreferences(EDIT, MODE_PRIVATE);
        return prefs.getInt(AGE, 0);

    }

    public String getPhoneValueFromEditProfile(Context context){

        SharedPreferences prefs = context.getSharedPreferences(EDIT, MODE_PRIVATE);
        return prefs.getString(PHONE, null);

    }

    public String getPicValueFromEditProfile(Context context){

        SharedPreferences prefs = context.getSharedPreferences(EDIT, MODE_PRIVATE);
        return prefs.getString(PIC, null);

    }

    public String getUuidStringValueFromShare(Context context){

        SharedPreferences prefs = context.getSharedPreferences(SHARE, MODE_PRIVATE);
        return prefs.getString(UUID, null);

    }




}

