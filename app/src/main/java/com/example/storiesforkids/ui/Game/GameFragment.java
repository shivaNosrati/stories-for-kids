package com.example.storiesforkids.ui.Game;

import android.content.Context;
import android.content.Intent;
import android.content.pm.ActivityInfo;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.FrameLayout;
import android.widget.ImageView;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.navigation.NavController;
import androidx.navigation.Navigation;
import androidx.recyclerview.widget.GridLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.example.storiesforkids.R;
import com.example.storiesforkids.ui.DrawerOptions.Fragment_CallToUs;
import com.example.storiesforkids.ui.MainView.MainActivity;
import com.example.storiesforkids.ui.Service.NetworkService;
import com.example.storiesforkids.ui.Share.SharePreferencesHelper;
import com.example.storiesforkids.ui.StoryText.GetGamesCallback;

import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;

public class GameFragment extends Fragment implements ClickOnGameItem {

    private String token;
    private GameAdapter gAdapter;

    @BindView(R.id.gameRecycler)
    RecyclerView gameRecycler;

    @BindView(R.id.gBack)
    ImageView back;

    private ImageView progressBar;
    Animation animation;
    private FrameLayout progressBarHolder;

    Fragment_CallToUs.DeleteSomeWidget deleteSomeWidget;

    NavController navController;
    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {

        View v = inflater.inflate(R.layout.games_fragment , container ,false);
        ((MainActivity)getActivity()).updateStatusBarColor("#19ABA0");
        ButterKnife.bind(this,v);
        token = SharePreferencesHelper.getInatance().getStringValueFromShare(getContext());


        navController = Navigation.findNavController(getActivity(), R.id.nav_host_fragment);
        back();

        return v;
    }


    private void back() {
        back.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                navController.navigate(R.id.mainFragment);
            }
        });

    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        deleteSomeWidget.goneToolbar();
        requestToGetGames();
        initProgressGeme(view);
    }


    @Override
    public void onAttach(@NonNull Context context) {
        super.onAttach(context);

        if(context instanceof Fragment_CallToUs.DeleteSomeWidget)
            this.deleteSomeWidget= (Fragment_CallToUs.DeleteSomeWidget) context;

    }

    private void requestToGetGames() {
        NetworkService.getService().getGames(token, new GetGamesCallback() {
            @Override
            public void gameReceived(GameModel data) {
                fillItems(data.getData());
               hideProgressGame();
            }

            @Override
            public void gameNotReceive(Throwable t) {

                Log.e("2729", ""+t);
                hideProgressGame();
            }
        });
    }

    private void fillItems(List<GameData> gList) {

        gAdapter = new GameAdapter(gList , getContext() , (GameFragment) this);
         RecyclerView.LayoutManager layoutManager = new GridLayoutManager(getActivity(),2);
        gameRecycler.setLayoutManager(layoutManager);
        gameRecycler.setAdapter(gAdapter);
    }

    @Override
    public void onResume() {
        super.onResume();
        if(getActivity() != null)
        getActivity().setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_PORTRAIT);

    }

    @Override
    public void onPause() {
        super.onPause();
        hideProgressGame();
        if(getActivity() != null)
        getActivity().setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_PORTRAIT);

    }


    @Override
    public void goToPlay(GameData data) {

        Intent i = new Intent(getContext(), WebViewClass.class);
        i.putExtra("game_link",data.getLink());
        startActivity(i);
    }

    private void initProgressGeme(View view) {
        progressBar = view.findViewById(R.id.progress);
        progressBarHolder = view.findViewById(R.id.progressBarHolder);
        animation = AnimationUtils.loadAnimation(getContext(), R.anim.clock_anim);
        animation.setRepeatCount(Animation.INFINITE);
        progressBarHolder.setVisibility(View.VISIBLE);
        progressBar.setVisibility(View.VISIBLE);
        progressBar.startAnimation(animation);

    }

    private void hideProgressGame() {

        progressBarHolder.setVisibility(View.GONE);
        progressBar.setVisibility(View.GONE);
        animation.cancel();

    }
}


