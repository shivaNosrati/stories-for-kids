package com.example.storiesforkids.ui.History;

public interface SetHistoryCallback {

    void historyIsSet();

    void historyNotSet(Throwable t);
}
