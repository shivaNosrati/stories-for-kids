package com.example.storiesforkids.ui.CheckNetwork;

public interface INetworkCallBack {

    void hasInternet();
    void noInternet();

}
