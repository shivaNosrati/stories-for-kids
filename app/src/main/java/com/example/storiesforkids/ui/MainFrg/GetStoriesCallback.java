package com.example.storiesforkids.ui.MainFrg;


import retrofit2.Call;

public interface GetStoriesCallback {

    void onReceived(InfoStoryModel infoStoryModel);

    void notReceived(Throwable t);

}
