package com.example.storiesforkids.ui.CheckNetwork;

import android.content.Context;


public interface IcheckNetworkConnectionUtils {


    boolean isNetworkConnected(Context context);

    void checkHasInternet(Context context, INetworkCallBack inetworkCallBack);

}
