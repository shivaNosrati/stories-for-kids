package com.example.storiesforkids.ui.MainView;

import androidx.fragment.app.Fragment;

public interface OnGetView {
    void onGetView(Fragment fragment);
}
