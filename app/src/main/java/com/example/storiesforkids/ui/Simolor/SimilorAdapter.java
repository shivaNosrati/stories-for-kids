package com.example.storiesforkids.ui.Simolor;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.bumptech.glide.Glide;
import com.example.storiesforkids.R;
import com.example.storiesforkids.ui.AllOfStoryInfo.DetailsStoryFragment;
import com.example.storiesforkids.ui.Favorite.Data;

import java.util.List;

public class SimilorAdapter extends RecyclerView.Adapter<SimilorAdapter.ViewHolder> {

    private List<Data> dataList;
    private Context context;
    private ClickOnSimImg clickonImage;


    public SimilorAdapter(List<Data> dataList , Context context , DetailsStoryFragment fragment) {

        this.dataList=dataList;
        this.context =context;
       this.clickonImage = fragment;

    }

    @NonNull
    @Override
    public SimilorAdapter.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View v = LayoutInflater.from(parent.getContext()).inflate(R.layout.similor_item, parent, false);
        SimilorAdapter.ViewHolder vh = new SimilorAdapter.ViewHolder(v);
        return vh;
    }

    @Override
    public void onBindViewHolder(@NonNull ViewHolder holder, int position) {

        String imgSimilor = dataList.get(position).getImageBookLink();
        String newString = imgSimilor.replace("https", "http");
        Glide.with(context).load(newString).into(holder.img_book);

        holder.img_book.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                clickonImage.goToDetailsClickedStory(dataList.get(position));
            }
        });


    }



    @Override
    public int getItemCount() {
        return dataList.size();
    }


    public class ViewHolder extends RecyclerView.ViewHolder {

       private ImageView img_book;


        public ViewHolder(View v) {
            super(v);

            img_book =  v.findViewById(R.id.img_sim_item);

        }
    }
}
