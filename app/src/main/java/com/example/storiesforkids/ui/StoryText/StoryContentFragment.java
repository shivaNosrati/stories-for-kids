package com.example.storiesforkids.ui.StoryText;

import android.content.Context;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.TextView;
import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.constraintlayout.widget.ConstraintLayout;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import com.example.storiesforkids.R;
import com.example.storiesforkids.ui.MainView.MainActivity;
import com.example.storiesforkids.ui.Service.NetworkService;
import com.example.storiesforkids.ui.Share.SharePreferencesHelper;
import com.example.storiesforkids.ui.PlayVoice.ParagraphData;
import com.example.storiesforkids.ui.PlayVoice.ParagraphModel;
import com.example.storiesforkids.ui.PlayVoice.Paragraphcallback;
import com.smarteist.autoimageslider.IndicatorAnimations;
import com.smarteist.autoimageslider.SliderAnimations;
import com.smarteist.autoimageslider.SliderView;
import java.util.List;
import butterknife.BindView;
import butterknife.ButterKnife;

public class StoryContentFragment extends Fragment {

    @BindView(R.id.textViewNameStory)
    TextView nameS;

    @BindView(R.id.imageSlider)
     SliderView sliderView;

    @BindView(R.id.storyTextRecycler)
    RecyclerView pRecycler;

    @BindView(R.id.main_layerText)
    ConstraintLayout layout;

    Integer id;
    String token;
    String stoyName;
    StoryContentAdapter aAdapter;
    private SliderAdapterStoryText adapter;
    private ImageView progressBar;
    Animation animation;
    private FrameLayout progressBarHolder;

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.content_story, container, false);
        ButterKnife.bind(this, view);
        ((MainActivity) getActivity()).updateStatusBarColor("#6B0BB5");



        getBasedata();
        setDetails();
        setStoryText();
        initProgressBar(view);


        return view;
    }


    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

    }

    private void getBasedata() {

        stoyName = getArguments().get("storyName").toString();
        Log.i("storyName", "" + stoyName);
        id = Integer.valueOf(getArguments().get("id").toString());
        token = SharePreferencesHelper.getInatance().getStringValueFromShare(getContext());
    }

    private void initProgressBar(View view) {

        progressBar = view.findViewById(R.id.progress_t);
        progressBarHolder = view.findViewById(R.id.progressBarHolder_t);
        animation = AnimationUtils.loadAnimation(getContext(), R.anim.clock_anim);
        animation.setRepeatCount(Animation.INFINITE);
        progressBarHolder.setVisibility(View.VISIBLE);
        progressBar.setVisibility(View.VISIBLE);
        progressBar.startAnimation(animation);


    }

    private void hideProgressBar() {

        progressBarHolder.setVisibility(View.GONE);
        progressBar.setVisibility(View.GONE);
        animation.cancel();
    }


    private void setStoryText() {

        NetworkService.getService().getParagraph(token, id, new Paragraphcallback() {
            @Override
            public void paragraghReseived(ParagraphModel paragraphModel) {

                fillText(paragraphModel.getData());
                fillSlider(paragraphModel.getData());
                hideProgressBar();


            }

            @Override
            public void paragraghNotReseived(Throwable t) {
                hideProgressBar();
            }
        });


    }



    private void fillSlider(List<ParagraphData> data) {
        adapter = new SliderAdapterStoryText(data, getContext());
        sliderView.setSliderAdapter(adapter);
        sliderView.setIndicatorAnimation(IndicatorAnimations.WORM); //set indicator animation by using SliderLayout.IndicatorAnimations. :WORM or THIN_WORM or COLOR or DROP or FILL or NONE or SCALE or SCALE_DOWN or SLIDE and SWAP!!
        sliderView.setSliderTransformAnimation(SliderAnimations.CUBEOUTSCALINGTRANSFORMATION);
    }

    private void fillText(List<ParagraphData> data) {
        aAdapter = new StoryContentAdapter(data, getContext());
        RecyclerView.LayoutManager layoutManager = new LinearLayoutManager(getActivity());
        pRecycler.setLayoutManager(layoutManager);
        pRecycler.setAdapter(aAdapter);
    }


    private void setDetails() {
        nameS.setText(stoyName);
    }


    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        if (getActivity() instanceof MainActivity) {

            ((MainActivity) getActivity()).deleteSomeUI();


        } else {
            throw new RuntimeException(context.toString()
                    + " must implement OnDelete");
        }
    }
}
