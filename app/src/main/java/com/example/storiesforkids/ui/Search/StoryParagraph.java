package com.example.storiesforkids.ui.Search;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class StoryParagraph {

    @SerializedName("number")
    @Expose
    private Integer number;
    @SerializedName("content")
    @Expose
    private String content;
    @SerializedName("image")
    @Expose
    private String image;
    @SerializedName("image_time")
    @Expose
    private String imageTime;

    public Integer getNumber() {
        return number;
    }

    public void setNumber(Integer number) {
        this.number = number;
    }

    public String getContent() {
        return content;
    }

    public void setContent(String content) {
        this.content = content;
    }

    public String getImage() {
        return image;
    }

    public void setImage(String image) {
        this.image = image;
    }

    public String getImageTime() {
        return imageTime;
    }

    public void setImageTime(String imageTime) {
        this.imageTime = imageTime;
    }

}