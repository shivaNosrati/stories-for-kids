package com.example.storiesforkids.ui.PlayVoice;

import android.app.ProgressDialog;
import android.content.Context;
import android.content.pm.ActivityInfo;
import android.media.AudioManager;
import android.media.MediaPlayer;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.Handler;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageButton;
import android.widget.SeekBar;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;

import androidx.fragment.app.Fragment;

import com.example.storiesforkids.R;
import com.example.storiesforkids.ui.DrawerOptions.Fragment_CallToUs;
import com.example.storiesforkids.ui.MainView.MainActivity;
import com.example.storiesforkids.ui.Service.NetworkService;
import com.example.storiesforkids.ui.Share.SharePreferencesHelper;
import com.smarteist.autoimageslider.IndicatorAnimations;
import com.smarteist.autoimageslider.SliderAnimations;
import com.smarteist.autoimageslider.SliderView;
import java.util.List;
import butterknife.BindView;
import butterknife.ButterKnife;



public class PlayVoiceFragment extends Fragment {


    @BindView(R.id.banner_slider1)
    SliderView sliderView;

    @BindView(R.id.buttonPlay)
    ImageButton playSound;

    @BindView(R.id.buttonPause)
    ImageButton pauseSound;

    @BindView(R.id.seekBar)
    SeekBar seekBar;

    VoiceSliderAdapter adapter;
    private int req_code = 129;
    String voice_uri;
    private Integer storyId;
    private boolean playPause;
    private MediaPlayer mediaPlayer;
    private ProgressDialog progressDialog;
    private boolean initialStage = true;

    Fragment_CallToUs.DeleteSomeWidget mListener;


    @Override
    public void onAttach(@NonNull Context context) {
        super.onAttach(context);
        if(context instanceof Fragment_CallToUs.DeleteSomeWidget)
            this.mListener= (Fragment_CallToUs.DeleteSomeWidget) context;
    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {

        View v = inflater.inflate(R.layout.fragment_play_voice, container , false);
        ((MainActivity)getActivity()).updateStatusBarColor("#6B0BB5");
        getActivity().setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_LANDSCAPE);

        ButterKnife.bind(this , v);

        getData();
        getParagraphList();
        setBanner();

//        ((MainActivity) getActivity()).deleteSomeUI();

        return v;
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        initPlay();
        initPause();
        initSeekBar();
        mListener.goneToolbar();

    }

    @Override
    public void onPause() {
        super.onPause();

        mediaPlayer.stop();
        mediaPlayer.pause();
        mSeekbarUpdateHandler.removeCallbacks(mUpdateSeekbar);


    }

    @Override
    public void onResume() {
        super.onResume();
    }



    private void getParagraphList() {
        String token = SharePreferencesHelper.getInatance().getStringValueFromShare(getContext());
        NetworkService.getService().getParagraph(token, storyId, new Paragraphcallback() {
            @Override
            public void paragraghReseived(ParagraphModel paragraphModel) {
                fillBanner(paragraphModel.getData());
            }

            @Override
            public void paragraghNotReseived(Throwable t) {

                Log.e("2729" , ""+t.getMessage());
            }
        });

    }

    private void fillBanner(List<ParagraphData> data) {
        VoiceSliderAdapter adapter = new VoiceSliderAdapter(data , getContext());
        sliderView.setSliderAdapter(adapter);
        sliderView.setIndicatorAnimation(IndicatorAnimations.WORM); //set indicator animation by using SliderLayout.IndicatorAnimations. :WORM or THIN_WORM or COLOR or DROP or FILL or NONE or SCALE or SCALE_DOWN or SLIDE and SWAP!!
        sliderView.setSliderTransformAnimation(SliderAnimations.CUBEINROTATIONTRANSFORMATION);

    }

    private void getData() {

        storyId =getArguments().getInt("sId");
        voice_uri = getArguments().getString("voice_link");
        mediaPlayer = new MediaPlayer();
        mediaPlayer.setAudioStreamType(AudioManager.STREAM_MUSIC);
        progressDialog = new ProgressDialog(getContext());
    }

    private void initSeekBar() {


        seekBar.setMax(mediaPlayer.getDuration());

        seekBar.setOnSeekBarChangeListener(new SeekBar.OnSeekBarChangeListener() {
            @Override
            public void onProgressChanged(SeekBar seekBar, int progress, boolean fromUser) {
                if (fromUser)
                    mediaPlayer.seekTo(progress);
            }

            @Override
            public void onStartTrackingTouch(SeekBar seekBar) {

            }

            @Override
            public void onStopTrackingTouch(SeekBar seekBar) {

            }
        });



    }

    private Handler mSeekbarUpdateHandler = new Handler();
    private Runnable mUpdateSeekbar = new Runnable() {
        @Override
        public void run() {

            seekBar.setProgress(mediaPlayer.getCurrentPosition());
            mSeekbarUpdateHandler.postDelayed(this, 50);
        }
    };


    private void setBanner() {

//        if (arrayList!= null){
//
//            SliderAdapterStoryText adapter = new SliderAdapterStoryText(arrayList , getContext());
//            sliderView.setSliderAdapter(adapter);
//            sliderView.setIndicatorAnimation(IndicatorAnimations.WORM); //set indicator animation by using SliderLayout.IndicatorAnimations. :WORM or THIN_WORM or COLOR or DROP or FILL or NONE or SCALE or SCALE_DOWN or SLIDE and SWAP!!
//            sliderView.setSliderTransformAnimation(SliderAnimations.CUBEINROTATIONTRANSFORMATION);
////            sliderView.setAutoCycleDirection(SliderView.AUTO_CYCLE_DIRECTION_BACK_AND_FORTH);
////            sliderView.setIndicatorSelectedColor(Color.WHITE);
////            sliderView.setIndicatorUnselectedColor(R.color.tPink);
////            sliderView.setScrollTimeInSec(4); //set scroll delay in seconds :
////            sliderView.startAutoCycle();
//        }

    }

    private void initPlay() {

        playSound.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (!playPause) {
                    if (initialStage) {
                        new Player().execute(voice_uri);
                    } else {
//                        if (!mediaPlayer.isPlaying()){
//
//                            mediaPlayer.start();
//                        }
                    }
                    playPause = true;
                    mediaPlayer.start();

                    mSeekbarUpdateHandler.postDelayed(mUpdateSeekbar, 0);

                } else {

                    if (mediaPlayer.isPlaying()) {
                        mediaPlayer.start();

                    }

                    playPause = false;
                }
            }

        });
    }

    private void initPause() {

        pauseSound.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (mediaPlayer.isPlaying()) {
                    mediaPlayer.pause();
                    mSeekbarUpdateHandler.removeCallbacks(mUpdateSeekbar);
                }

                playPause = false;
            }
        });


    }


    class Player extends AsyncTask<String, Void, Boolean> {
        @Override
        protected Boolean doInBackground(String... strings) {
            Boolean prepared = false;


            try {
                mediaPlayer.setDataSource(strings[0]);
                mediaPlayer.setOnCompletionListener(new MediaPlayer.OnCompletionListener() {
                    @Override
                    public void onCompletion(MediaPlayer mediaPlayer) {
                        initialStage = true;
                        playPause = false;
                        playSound.setBackgroundResource(R.drawable.exo_controls_play);
                        mediaPlayer.stop();
                        mediaPlayer.reset();

                    }
                });

                mediaPlayer.prepare();
                prepared = true;

            } catch (Exception e) {
                Log.e("MyAudioStreamingApp", e.getMessage()+" ");
                prepared = false;
            }

            return prepared;
        }

        @Override
        protected void onPostExecute(Boolean aBoolean) {
            super.onPostExecute(aBoolean);
            if (progressDialog.isShowing()) {
                progressDialog.cancel();
            }

            mediaPlayer.start();
            initSeekBar();
            initialStage = false;
        }

        @Override
        protected void onPreExecute() {
            super.onPreExecute();

            progressDialog.setMessage("صبور باشید...");
            progressDialog.show();
        }
    }

}


