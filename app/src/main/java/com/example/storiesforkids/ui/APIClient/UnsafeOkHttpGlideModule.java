package com.example.storiesforkids.ui.APIClient;

import android.content.Context;
import com.bumptech.glide.Glide;
import com.bumptech.glide.GlideBuilder;
import com.bumptech.glide.Registry;
import com.bumptech.glide.integration.okhttp3.OkHttpUrlLoader;
import com.bumptech.glide.load.DecodeFormat;
import com.bumptech.glide.load.model.GlideUrl;
import com.bumptech.glide.module.AppGlideModule;
import com.bumptech.glide.request.RequestOptions;
import java.io.InputStream;
import okhttp3.OkHttpClient;


public class UnsafeOkHttpGlideModule extends AppGlideModule {
    @Override
    public void registerComponents(Context context, Glide glide, Registry registry) {
        OkHttpClient client = RetrofitInstance.getUnsafeOkHttpClient();

        registry.replace(GlideUrl.class, InputStream.class,
                new OkHttpUrlLoader.Factory(client));

    }

    @Override
    public void applyOptions(Context context, GlideBuilder builder) {
        builder.setDefaultRequestOptions(new RequestOptions().format(DecodeFormat.PREFER_ARGB_8888));

    }
    @Override
    public boolean isManifestParsingEnabled() {
        return false;
    }
}
