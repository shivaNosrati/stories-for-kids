package com.example.storiesforkids.ui;

import android.app.Application;
import android.content.Context;

import com.downloader.PRDownloader;
import com.downloader.PRDownloaderConfig;
import com.example.storiesforkids.R;
import uk.co.chrisjenx.calligraphy.CalligraphyConfig;


public class App extends Application {

    public static Context context;
    @Override
    public void onCreate() {
        super.onCreate();
        context = getApplicationContext();
        CalligraphyConfig.initDefault(new CalligraphyConfig.Builder()
                .setDefaultFontPath("fonts/font_iran.ttf")
                .setFontAttrId(R.attr.fontPath)
                .build()
        );

        PRDownloaderConfig config = PRDownloaderConfig.newBuilder()
                .setDatabaseEnabled(true)
                .build();
        PRDownloader.initialize(this, config);


    }

    }





