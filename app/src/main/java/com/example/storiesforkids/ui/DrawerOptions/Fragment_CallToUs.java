package com.example.storiesforkids.ui.DrawerOptions;

import android.content.Context;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import androidx.activity.OnBackPressedCallback;
import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.navigation.NavController;
import androidx.navigation.Navigation;

import com.example.storiesforkids.R;
import com.example.storiesforkids.ui.MainView.MainActivity;
import com.example.storiesforkids.ui.MainView.OnGetView;
import com.example.storiesforkids.ui.MainView.SomeChangeInView;

import butterknife.BindView;
import butterknife.ButterKnife;

public class Fragment_CallToUs extends Fragment {

    @BindView(R.id.btn_enteghad)
    Button save;

    @BindView(R.id.editText)
    EditText myText;

    DeleteSomeWidget viewListener;
    private NavController navController;

    public interface DeleteSomeWidget{
        void goneToolbar();
    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        ((MainActivity)getActivity()).updateStatusBarColor("#8FBB12");

        navController = Navigation.findNavController(getActivity(), R.id.nav_host_fragment);

        View v = inflater.inflate(R.layout.fragment_call , container ,false);
        ButterKnife.bind(this , v);
        return v;

    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        viewListener.goneToolbar();
        sendNazar();
    }

    private void sendNazar() {
        save.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Toast.makeText(getContext(), "با تشکر از همراهی شما ", Toast.LENGTH_SHORT).show();


            }
        });
    }

    @Override
    public void onAttach(@NonNull Context context) {
        super.onAttach(context);

        if (context instanceof DeleteSomeWidget){
            this.viewListener = (DeleteSomeWidget) context;

        }

    }
}
