package com.example.storiesforkids.ui.MainFrg;


import android.animation.AnimatorSet;
import android.animation.ObjectAnimator;
import android.annotation.TargetApi;
import android.content.Context;
import android.os.Build;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;
import androidx.annotation.NonNull;
import androidx.constraintlayout.widget.ConstraintLayout;
import androidx.recyclerview.widget.RecyclerView;
import com.bumptech.glide.Glide;
import com.example.storiesforkids.R;
import com.example.storiesforkids.ui.Favorite.Data;
import java.util.List;


public class AllStoriesAdapter extends RecyclerView.Adapter<AllStoriesAdapter.ViewHolder> {

	private List<Data> dataList;
	private ClickOnButtonListen clickOnButtonListen;
	private ClickOnButtonRead clickOnButtonRead;
	private Context context;
	private ClickOnButtonInterface clickOnButtonInterface;
	private ClickForShareElement clickForShareElement;
	long DURATION = 100;
	private boolean on_attach = true;


	public AllStoriesAdapter(List<Data> dataList , Context context , MainFragment mainFragment) {

		this.dataList=dataList;
		this.context =context;

		this.clickOnButtonRead =  mainFragment;
		this.clickOnButtonListen =  mainFragment;
		this.clickForShareElement =  mainFragment;


	}

	@NonNull
	@Override
	public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
		View v = LayoutInflater.from(parent.getContext()).inflate(R.layout.stories_items, parent, false);
		ViewHolder vh = new ViewHolder(v);
		return vh;
	}

	@Override
	public void onBindViewHolder(ViewHolder holder, int position) {
		holder.bookTitle.setText(String.valueOf(dataList.get(position).getName()));
		holder.input_writer.setText(dataList.get(position).getWriter());
		holder.input_illustrator.setText(dataList.get(position).getDesigner());

		String img = dataList.get(position).getImageBookLink();
		String newString = img.replace("https", "http");

	    Glide.with(context).load(newString).into(holder.img_book);


		if (dataList.get(position).getVoiceLink() == null ||dataList.get(position).getVoiceLink().equals("")){
			holder.btn_voice.setVisibility(View.INVISIBLE);
			holder.btn_voice.setClickable(false);
		}



		holder.btn_text.setOnClickListener(new View.OnClickListener() {
			@Override
			public void onClick(View view) {

				clickOnButtonRead.doSumThingForRead(dataList.get(position));

			}
		});
		holder.btn_voice.setOnClickListener(new View.OnClickListener() {
			@Override
			public void onClick(View view) {

				clickOnButtonListen.doSumThingForListen(dataList.get(position));

			}
		});

/*		holder.bookTitle.setOnClickListener(new View.OnClickListener() {
			@TargetApi(Build.VERSION_CODES.LOLLIPOP)
			@Override
			public void onClick(View view) {

				clickOnButtonInterface.doSumThingForAdapter(dataList.get(position ) ,dataList.get(position).getImageBookLink());

			}
		});*/


		holder.img_book.setOnClickListener(new View.OnClickListener() {
			@TargetApi(Build.VERSION_CODES.LOLLIPOP)
			@Override
			public void onClick(View view) {

				clickForShareElement.initTransitionAnimation(dataList.get(position),holder,position);

			}
		});

		FromLeftToRight(holder.itemView, position);
	}



	@Override
	public void onAttachedToRecyclerView(@NonNull RecyclerView recyclerView) {


		recyclerView.addOnScrollListener(new RecyclerView.OnScrollListener() {
			@Override
			public void onScrollStateChanged(@NonNull RecyclerView recyclerView, int newState) {
				Log.d("tag", "onScrollStateChanged: Called " + newState);
				on_attach = false;
				super.onScrollStateChanged(recyclerView, newState);
			}
		});
		super.onAttachedToRecyclerView(recyclerView);
	}

	@Override
	public int getItemCount() {
		return dataList.size();
	}


	private void FromLeftToRight(View itemView, int position) {
		if(!on_attach){
			position = -1;
		}
		boolean not_first_item = position == -1;
		position = position + 1;
		itemView.setTranslationX(-400f);
		itemView.setAlpha(0.f);
		AnimatorSet animatorSet = new AnimatorSet();
		ObjectAnimator animatorTranslateY = ObjectAnimator.ofFloat(itemView, "translationX", -400f, 0);
		ObjectAnimator animatorAlpha = ObjectAnimator.ofFloat(itemView, "alpha", 1.f);
		ObjectAnimator.ofFloat(itemView, "alpha", 0.f).start();
		animatorTranslateY.setStartDelay(not_first_item ? DURATION : (position * DURATION));
		animatorTranslateY.setDuration((not_first_item ? 2 : 1) * DURATION);
		animatorSet.playTogether(animatorTranslateY, animatorAlpha);
		animatorSet.start();
	}

	public class ViewHolder extends RecyclerView.ViewHolder {

		ImageView img_book;
		Button btn_voice;
		Button btn_text;
		TextView bookTitle;
		TextView writerName;
		TextView illustrator;
		TextView input_writer;
		TextView input_illustrator;
		ConstraintLayout layout;

		public ViewHolder(View v) {
			super(v);

			layout =v.findViewById(R.id.rLayout);
			img_book =  v.findViewById(R.id.img_book);
			bookTitle =  v.findViewById(R.id.bookTitle);
			writerName =  v.findViewById(R.id.writerName);
			illustrator =  v.findViewById(R.id.illustrator);
			input_writer =  v.findViewById(R.id.input_writer);
			input_illustrator =  v.findViewById(R.id.input_illustrator);
			btn_voice =  v.findViewById(R.id.btn_voice);
			btn_text =  v.findViewById(R.id.btn_text);

		}
	}

}
