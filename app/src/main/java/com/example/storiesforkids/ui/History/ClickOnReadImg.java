package com.example.storiesforkids.ui.History;

import com.example.storiesforkids.ui.Favorite.Data;

public interface ClickOnReadImg {

    void goToDetails(HistoryData infoStory);
}
