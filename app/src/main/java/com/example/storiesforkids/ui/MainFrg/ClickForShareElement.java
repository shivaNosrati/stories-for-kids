package com.example.storiesforkids.ui.MainFrg;


import com.example.storiesforkids.ui.Favorite.Data;

public interface ClickForShareElement {
    void initTransitionAnimation(Data infoStory, AllStoriesAdapter.ViewHolder view , int position);
}
