package com.example.storiesforkids.ui.MainView;

import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.graphics.Color;
import android.os.Build;
import android.os.Bundle;

import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import androidx.constraintlayout.widget.ConstraintLayout;
import androidx.core.view.GravityCompat;
import androidx.fragment.app.Fragment;
import androidx.navigation.NavController;
import androidx.navigation.NavOptions;
import androidx.navigation.Navigation;
import androidx.navigation.ui.NavigationUI;
import androidx.drawerlayout.widget.DrawerLayout;

import com.bumptech.glide.Glide;
import com.example.storiesforkids.R;
import com.example.storiesforkids.ui.BaseActivity;
import com.example.storiesforkids.ui.BaseFragment;
import com.example.storiesforkids.ui.Comments.CommentFragment;
import com.example.storiesforkids.ui.DrawerOptions.Fragment_CallToUs;
import com.example.storiesforkids.ui.Favorite.ClickOnFaveItem;
import com.example.storiesforkids.ui.Favorite.FaveData;
import com.example.storiesforkids.ui.Game.GameFragment;
import com.example.storiesforkids.ui.History.ClickOnReadImg;
import com.example.storiesforkids.ui.History.HistoryData;
import com.example.storiesforkids.ui.Service.NetworkService;
import com.example.storiesforkids.ui.Share.SharePreferencesHelper;
import com.example.storiesforkids.ui.UserInformation.FragmentProfile;
import com.example.storiesforkids.ui.Favorite.FavoriteFragment;
import com.example.storiesforkids.ui.MainFrg.MainFragment;
import com.example.storiesforkids.ui.Category.CategoryFragment;
import com.example.storiesforkids.ui.Search.SearchFragment;
import com.example.storiesforkids.ui.UserInformation.IbackPressListener;
import com.example.storiesforkids.ui.UserInformation.UserInfoCallBack;
import com.example.storiesforkids.ui.UserInformation.UserInfoModel;
import com.google.android.material.navigation.NavigationView;

import butterknife.BindView;
import butterknife.ButterKnife;
import de.hdodenhof.circleimageview.CircleImageView;

public class MainActivity extends BaseActivity implements OnGetView,CommentFragment.OnFragmentComment,   SomeChangeInView, ClickOnFaveItem, ClickOnReadImg , FragmentProfile.GetGhange , Fragment_CallToUs.DeleteSomeWidget , SearchFragment.DeleteSomeWidgetForSearch , MainFragment.changeClickable {

    IbackPressListener backPressListener;
    private   ConstraintLayout clButton;
    private   ConstraintLayout m_toolbar;
    private   ImageView icon3;
    @BindView(R.id.navigationView)
    NavigationView navigationView;
    @BindView(R.id.ssearch)
    ImageView search;
    @BindView(R.id.con_header)
    ConstraintLayout mtAccount;

    @BindView(R.id.lc1)
    ConstraintLayout lc1;

    @BindView(R.id.lc2)
    ConstraintLayout lc2;

    @BindView(R.id.lc3)
    ConstraintLayout lc3;

    @BindView(R.id.tMenu2)
    TextView item2;
    @BindView(R.id.tMenu3)
    TextView item3;
    @BindView(R.id.tMenu4)
    TextView item4;
    @BindView(R.id.tMenu5)
    TextView item5;
    @BindView(R.id.drawerIcon)
    ImageView icon;
    @BindView(R.id.drawer_layout)
    DrawerLayout drawerLayout;
    @BindView(R.id.icon1)
    ImageView icon1;
    @BindView(R.id.icon2)
    ImageView icon2;
    @BindView(R.id.create_profile)
    CircleImageView profile;


    private NavController navController;
    private ProgressDialog progressDialog;
    private String token;



    @Override
    protected void onCreate(Bundle savedInstanceState) {

        super.onCreate(savedInstanceState);

        setContentView(R.layout.activity_main);
        ButterKnife.bind(this );

         token= SharePreferencesHelper.getInatance().getStringValueFromShare(getApplicationContext());

        clButton = findViewById(R.id.custom_bottom );
        m_toolbar = findViewById(R.id.main_toolbar);
        icon3= findViewById(R.id.icon3);


       try{

            navController = Navigation.findNavController(this, R.id.nav_host_fragment);

        }catch (Exception e){
            e.printStackTrace();
        }

        openDrawer();
        initNavButton();
        initSearch();
        initDrawerMenu();
        getImageProfile(token);

    }


    private void getImageProfile(String token) {

            NetworkService.getService().userInformation(token, new UserInfoCallBack() {
                @Override
                public void userInfoIsAvailable(UserInfoModel infoList) {

                    String img = infoList.getData().getPic();
                    String newString = img.replace("https", "http");
                    SharePreferencesHelper.getInatance().insertEditValue(getApplicationContext() , 1);
                    if (infoList.getData().getPic() == null)
                        profile.setImageResource(R.drawable.profile);
                    else{
                        Glide.with(getApplicationContext())
                                .load(newString)
                                .into(profile);
                        //.transition(withCrossFade(R.anim.fade_in, 300));
                        profile.setRotation(0);
                    }


                }

                @Override
                public void infoNotAvailable(Throwable t) {

                    profile.setImageResource(R.drawable.profile);

                }
            });

    }

    public void updateStatusBarColor(String color){// Color must be in hexadecimal fromat
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
                Window window = getWindow();
                window.addFlags(WindowManager.LayoutParams.FLAG_DRAWS_SYSTEM_BAR_BACKGROUNDS);
                window.setStatusBarColor(Color.parseColor(color));
            }
        }

    private void initDrawerMenu() {
        NavOptions navOptions = new NavOptions.Builder().setPopUpTo(R.id.mainFragment,false).build();


        mtAccount.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                clButton.setVisibility(View.GONE);
                m_toolbar.setVisibility(View.GONE);

                navController.navigate(R.id.fragmentProfile,null,navOptions);
                drawerLayout.closeDrawers();



            }
        });

        item2.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                Intent sharingIntent = new Intent(android.content.Intent.ACTION_SEND);
                sharingIntent.setType("text/plain");
                String shareBody = "Here is the share content body";
                sharingIntent.putExtra(android.content.Intent.EXTRA_SUBJECT, "Subject Here");
                sharingIntent.putExtra(android.content.Intent.EXTRA_TEXT, shareBody);
                startActivity(Intent.createChooser(sharingIntent, "Share via"));

            }
        });
        item3.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                drawerLayout.closeDrawers();
                try{

                   navController.navigate(R.id.fragment_CallToUs,null,navOptions);



                }catch (Exception e){
                    e.printStackTrace();
                }

//                NavOptions navOptions = new NavOptions.Builder().setPopUpTo(R.id.mainFragment ,true).build();
////                final  NavController navController1 = Navigation.findNavController(view);
//                navController.navigate(R.id.fragment_CallToUs , null , navOptions);

            }
        });
        item4.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                drawerLayout.closeDrawers();
                try{

                    navController.navigate(R.id.gameFragment,null,navOptions);


                }catch (Exception e){
                    e.printStackTrace();
                }

//                NavOptions navOptions = new NavOptions.Builder().setPopUpTo(R.id.mainFragment ,true).build();
//         //       final  NavController navController1 = Navigation.findNavController(view);
//                navController.navigate(R.id.gameFragment , null , navOptions);


            }
        });
        item5.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {


            }
        });

    }

    @Override
    public void onBackPressed() {
        if (drawerLayout.isDrawerOpen(GravityCompat.END)) {
            drawerLayout.closeDrawer(GravityCompat.END);

        } else {
                super.onBackPressed();
        }



//        Fragment fragment = getSupportFragmentManager().findFragmentById(R.id.fragmentProfile);
//        if ((fragment instanceof OnBackPressedListener) || !((OnBackPressedListener) fragment).onBackPressed()) {
//            super.onBackPressed();
//        }



//        else {
//     //    getSupportFragmentManager().popBackStackImmediate();
//            navController.popBackStack(R.id.mainFragment , true);
//
//        //    getSupportFragmentManager().popBackStack(null, FragmentManager.POP_BACK_STACK_INCLUSIVE);
//      //      navController.navigate(R.id.mainFragment);
//
//         //   getSupportFragmentManager().popBackStackImmediate(null, FragmentManager.POP_BACK_STACK_INCLUSIVE);
//
//
//
//        }

        }

    private void initSearch() {

        search.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
               navController.navigate(R.id.fragment_Search);
            }
        });
    }

    private void openDrawer() {

        icon.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (drawerLayout.isDrawerOpen(GravityCompat.END)) {
                    drawerLayout.closeDrawer(GravityCompat.END);


                } else {
                    drawerLayout.openDrawer(GravityCompat.END);
                    drawerLayout.setDrawerLockMode(DrawerLayout.LOCK_MODE_UNLOCKED);

                }
            }
        });

    }

    @Override
    public boolean onSupportNavigateUp() {

        return  NavigationUI.navigateUp(Navigation.findNavController(this, R.id.nav_host_fragment), drawerLayout);
    }

    private void initNavButton() {

        NavOptions navOptions1 = new NavOptions.Builder().setPopUpTo(R.id.mainFragment,false).build();


        icon1.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                ImageView image = view.findViewById(R.id.icon1);
                Animation animation1 =
                        AnimationUtils.loadAnimation(getApplicationContext(),
                                R.anim.clockwise);
                image.startAnimation(animation1);
                navController.navigate(R.id.readFrg);

//                navController.navigate(R.id.readFrg,null,navOptions1);

            }
        });
        icon2.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                ImageView image = view.findViewById(R.id.icon2);
                Animation animation2 =
                        AnimationUtils.loadAnimation(getApplicationContext(),
                                R.anim.clockwise);
                image.startAnimation(animation2);

               navController.navigate(R.id.favoriteFragment);

//                navController.navigate(R.id.favoriteFragment,null,navOptions1);

            }
        });
        icon3.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                ImageView image = view.findViewById(R.id.icon3);
                Animation animation3 =
                        AnimationUtils.loadAnimation(getApplicationContext(),
                                R.anim.clockwise);
                image.startAnimation(animation3);
                icon3.setClickable(true);
                navController.navigate(R.id.mainFragment);
//                navController.navigate(R.id.mainFragment,null,navOptions1);

            }
        });

    }

    @Override
    public void onGetView(Fragment fragment) {


        if (fragment instanceof MainFragment){

            m_toolbar.setBackgroundResource(R.color.main_tool);
            lc1.setBackgroundResource(R.color.white);

            lc2.setBackgroundResource(R.color.white);

            lc3.setBackgroundResource(R.color.lcColor);

        }
        if (fragment instanceof FavoriteFragment){

            m_toolbar.setBackgroundResource(R.color.toolbarPink);
            lc1.setBackgroundResource(R.color.white);

            lc2.setBackgroundResource(R.color.lcColor);

            lc3.setBackgroundResource(R.color.white);

        }
        if(fragment instanceof CategoryFragment){

            m_toolbar.setBackgroundResource(R.color.blueColor);

            lc1.setBackgroundResource(R.color.lcColor);

            lc2.setBackgroundResource(R.color.white);

            lc3.setBackgroundResource(R.color.white);

        }
        if(fragment instanceof SearchFragment){

            goneUi();

        }

        if(fragment instanceof GameFragment){

            goneUi();
        }



    }

    @Override
    public void deleteSomeUI() {
        clButton.setVisibility(View.GONE);
        m_toolbar.setVisibility(View.GONE);

    }

    @Override
    public void onFragmentChangeListener(BaseFragment fragment) {
        goneUi();
        backPressListener = fragment;
    }

    public void goneUi(){
        clButton.setVisibility(View.GONE);
        m_toolbar.setVisibility(View.GONE);

    }

    @Override
    public void visibleUi() {

        clButton.setVisibility(View.VISIBLE);
        m_toolbar.setVisibility(View.VISIBLE);
    }


    @Override
    public void goToDetailsClickedFave(FaveData infoStory) {
        Bundle dataBundle = new Bundle();
        dataBundle.putInt("id" , infoStory.getId());
        dataBundle.putString("designer" ,infoStory.getDesigner());
        dataBundle.putString("storyName" ,infoStory.getName());
        dataBundle.putString("Talker" ,infoStory.getTalker());
        dataBundle.putString("storyTitle" ,infoStory.getTitle());
        dataBundle.putString("writer" ,infoStory.getWriter());
        dataBundle.putString("img" ,infoStory.getImageBookLink());
        dataBundle.putString("voice_link" , infoStory.getVoiceLink());
        dataBundle.putBoolean("isLiked" , infoStory.getIs_like());

        navController.navigate(R.id.detailsStoryFragment , dataBundle);
    }

    @Override
    public void goToDetails(HistoryData infoStory) {
        Bundle dataBundle = new Bundle();
        dataBundle.putInt("id" , infoStory.getId());
        dataBundle.putString("designer" ,infoStory.getDesigner());
        dataBundle.putString("storyName" ,infoStory.getName());
        dataBundle.putString("Talker" ,infoStory.getTalker());
        dataBundle.putString("storyTitle" ,infoStory.getTitle());
        dataBundle.putString("writer" ,infoStory.getWriter());
        dataBundle.putString("img" ,infoStory.getImageBookLink());
        dataBundle.putString("voice_link" , infoStory.getVoiceLink());
        dataBundle.putBoolean("isLiked" , infoStory.getIs_like());

        navController.navigate(R.id.detailsStoryFragment , dataBundle);
    }

    @Override
    public void isGhanged() {
        String token = SharePreferencesHelper.getInatance().getStringValueFromShare(getApplicationContext());
      //  Toast.makeText(this, "profile changed", Toast.LENGTH_SHORT).show();
        getImageProfile(token);
    }

    @Override
    public void goneToolbar() {
        clButton.setVisibility(View.GONE);
        m_toolbar.setVisibility(View.GONE);
    }

    @Override
    public void hideSomWidget() {
        clButton.setVisibility(View.GONE);
        m_toolbar.setVisibility(View.GONE);
    }


    @Override
    public void setChangeInClickable() {
        icon3.setClickable(true);
    }
}
