package com.example.storiesforkids.ui.MainFrg;

import android.content.Context;
import android.content.pm.ActivityInfo;
import android.os.Build;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.Toast;
import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.annotation.RequiresApi;
import androidx.constraintlayout.widget.ConstraintLayout;
import androidx.fragment.app.Fragment;
import androidx.lifecycle.MutableLiveData;
import androidx.lifecycle.Observer;
import androidx.lifecycle.ViewModelProviders;
import androidx.navigation.NavController;
import androidx.navigation.NavOptions;
import androidx.navigation.Navigation;
import androidx.navigation.fragment.FragmentNavigator;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import com.example.storiesforkids.R;
import com.example.storiesforkids.ui.BaseFragment;
import com.example.storiesforkids.ui.Favorite.Data;
import com.example.storiesforkids.ui.History.SetHistoryCallback;
import com.example.storiesforkids.ui.MainView.MainActivity;
import com.example.storiesforkids.ui.MainView.OnGetView;
import com.example.storiesforkids.ui.Service.NetworkService;
import com.example.storiesforkids.ui.Share.SharePreferencesHelper;
import com.example.storiesforkids.ui.UserInformation.UserInfo;
import com.example.storiesforkids.ui.mvvm.SViewModel;
import java.util.List;
import java.util.Objects;
import butterknife.BindView;
import butterknife.ButterKnife;
import static com.example.storiesforkids.ui.mvvm.SViewModel.mutableLiveData;


public class MainFragment extends BaseFragment implements  ClickOnButtonRead, ClickOnButtonListen, ClickForShareElement {


    @BindView(R.id.StoryRecyclerView)
    RecyclerView recyclerView;

    @BindView(R.id.main_frg_layout)
    ConstraintLayout layout;


    private boolean hasInternet = false;
    private String token;
    private ImageView progressBar;
    private Animation animation;
    private FrameLayout progressBarHolder;
    private AllStoriesAdapter adapter;
    private NavController navController;
    private NavOptions navOp;
    private changeClickable clickableListener;
    private OnGetView getVieListener;
    private SViewModel mainViewModel;
    private  static MutableLiveData<List<Data>> storyLiveData;
    private  static List<Data> storyLiveData1;



    public interface changeClickable{
        void setChangeInClickable();
    }

    @Override
    public void onAttach(@NonNull Context context) {
        super.onAttach(context);
        if (context instanceof changeClickable)
            this.clickableListener = (changeClickable) context;
            this.getVieListener = (OnGetView) context;


    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_main , container , false);
        ButterKnife.bind(this , view);
        navController = Navigation.findNavController(getActivity(), R.id.nav_host_fragment);
        navOp = new NavOptions.Builder().setPopUpTo(R.id.mainFragment,false).build();
        progressBar = view.findViewById(R.id.progress);

        ((MainActivity)getActivity()).updateStatusBarColor("#8FBB12");

        token = SharePreferencesHelper.getInatance().getStringValueFromShare(getContext());

        progressBarHolder = view.findViewById(R.id.progressBarHolder);

       return view;
    }


    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

    //    initProgress(view);
//
//        CheckNetworkConnectionUtils.getInstance().checkHasInternet(getContext(), new INetworkCallBack() {
//
//            @Override
//            public void hasInternet() {
//                hasInternet = true;
//                sendRequestToGetStories(token);
//                initProgress(view);
//
//            }
//
//            @Override
//            public void noInternet() {
//                hasInternet=true;
//
//                new AlertDialog.Builder(getContext())
//                        .setTitle("خطای اینترنت")
//                        .setMessage("اینترنت گوشیتو روشن کن و دوباره تلاش کن")
//
//                        .setPositiveButton(android.R.string.yes, new DialogInterface.OnClickListener() {
//                            public void onClick(DialogInterface dialog, int which) {
//                                startActivity(new Intent(Settings.ACTION_DATA_USAGE_SETTINGS));
//
//                            }
//                        })
//                        // A null listener allows the button to dismiss the dialog and take no further action.
//                        .setIcon(android.R.drawable.ic_dialog_alert)
//                        .show();
//
//            }
//        });
    }


    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
//        sendRequestToGetStories(getContext() ,token);
        getPopularStories();
    }

    private void getPopularStories() {
        initProgress();

            if (storyLiveData1 != null && mutableLiveData!= null){
                    Log.e("mutableListsize" , "mutable Listsize is"+ Objects.requireNonNull(mutableLiveData.getValue()).size());
                    List<Data> sList = storyLiveData.getValue();
                    fillStoriesLis(sList);
                    hideProgressBar();
                    Log.i("ggggggggg","size1  : "+ Objects.requireNonNull(storyLiveData.getValue()).size());

            }else{
                mainViewModel = ViewModelProviders.of(Objects.requireNonNull(getActivity())).get(SViewModel.class);
                mainViewModel.getAllStories().observe(this, new Observer<List<Data>>() {
                @Override
                public void onChanged(@Nullable List<Data> blogList) {
                    hideProgressBar();
                    fillStoriesLis(blogList);
                    storyLiveData   = new MutableLiveData<>();
                    storyLiveData.setValue(blogList);
                    storyLiveData1=storyLiveData.getValue();
                    Log.i("ggggggggg","size  : "+storyLiveData.getValue().size());
                    Log.i("ggggggggg","size  : "+storyLiveData1.size());
                }
            });}
    }

    @Override
    public void onResume() {
        super.onResume();

        getVieListener.onGetView(this);
        if(getActivity() != null)
            getActivity().setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_PORTRAIT);
    }

    @Override
    public void onPause() {
        super.onPause();

        if(getActivity() != null)
            getActivity().setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_PORTRAIT);

    }

    @Override
    public void onStart() {

        super.onStart();
        if (getActivity() instanceof MainActivity) {

            ((MainActivity) getActivity()).visibleUi();


        } else {
            throw new RuntimeException(getContext().toString()
                    + " must implement OnDelete");
        }
    }

    private void initProgress() {


        animation = AnimationUtils.loadAnimation(getContext(), R.anim.clock_anim);
        animation.setRepeatCount(Animation.INFINITE);
        progressBarHolder.setVisibility(View.VISIBLE);
        progressBar.setVisibility(View.VISIBLE);
        progressBar.startAnimation(animation);

    }

    private void hideProgressBar() {

        progressBarHolder.setVisibility(View.GONE);
        progressBar.setVisibility(View.GONE);
        animation.cancel();

    }

    private void sendRequestToGetStories(String token) {
        NetworkService.getService().getStories( token, new GetStoriesCallback()
        {
            @Override
            public void onReceived(InfoStoryModel infoStoryModel) {
              //  hideProgressBar();
                clickableListener.setChangeInClickable();
//                fillStoriesLis(infoStoryModel.getData());

            }
            @Override
            public void notReceived(Throwable t) {
                clickableListener.setChangeInClickable();
              //  hideProgressBar();

            }

        });
    }

    private void fillStoriesLis(List<Data> dList) {

        adapter = new AllStoriesAdapter(dList , getContext() , this);
        RecyclerView.LayoutManager layoutManager = new LinearLayoutManager(getActivity());
        // RecyclerView.LayoutManager layoutManager = new GridLayoutManager(getActivity(),2);
        recyclerView.setLayoutManager(layoutManager);
        recyclerView.setAdapter(adapter);
//        recyclerView.getViewTreeObserver().addOnPreDrawListener(
//                new ViewTreeObserver.OnPreDrawListener() {
//
//                    @Override
//                    public boolean onPreDraw() {
//                        recyclerView.getViewTreeObserver().removeOnPreDrawListener(this);
//
//                        for (int i = 0; i < recyclerView.getChildCount(); i++) {
//                            View v = recyclerView.getChildAt(i);
//                            v.setAlpha(0.0f);
//                            v.animate().alpha(1.0f)
//                                    .setDuration(300)
//                                    .setStartDelay(i * 50)
//                                    .start();
//                        }
//
//                        return true;
//                    }
//                });



    }

//    @Override
//    public void doSumThingForAdapter(Data infoStory, String imageView) {
//
//        Bundle dataBundle = new Bundle();
//        dataBundle.putInt("id" , infoStory.getId());
//        dataBundle.putString("designer" ,infoStory.getDesigner());
//        dataBundle.putString("storyName" ,infoStory.getName());
//        dataBundle.putString("Talker" ,infoStory.getTalker());
//        dataBundle.putString("storyTitle" ,infoStory.getTitle());
//        dataBundle.putString("writer" ,infoStory.getWriter());
//        dataBundle.putString("img" ,infoStory.getImageBookLink());
//        dataBundle.putString("voice" , infoStory.getVoiceLink());
//        dataBundle.putBoolean("isLiked" , infoStory.getIs_like());
//
//       navController.navigate(R.id.detailsStoryFragment  ,dataBundle , navOp);
//
//
//    }

    @Override
    public void doSumThingForRead(Data infoStory) {
        Bundle bundle = new Bundle();
        bundle.putString("storyName" ,infoStory.getName());
        bundle.putInt("id" , infoStory.getId());
        bundle.putString("img" , infoStory.getImageBookLink());

        String token = SharePreferencesHelper.getInatance().getStringValueFromShare(getContext());
        NetworkService.getService().setHis(infoStory.getId(), token, new SetHistoryCallback() {
            @Override
            public void historyIsSet() {

            }

            @Override
            public void historyNotSet(Throwable t) {
                Toast.makeText(getContext(), ""+t.getMessage(), Toast.LENGTH_SHORT).show();
            }
        });

        navController.navigate(R.id.storyContext2 , bundle ,navOp);
    }

    @Override
    public void doSumThingForListen(Data dataStory) {
        Bundle bundle = new Bundle();
        bundle.putString("voice_link" , dataStory.getVoiceLink());
        bundle.putInt("sId" ,dataStory.getId());

        navController.navigate(R.id.playVoiceFragment , bundle ,navOp);


    }


    @RequiresApi(api = Build.VERSION_CODES.LOLLIPOP)
    @Override
    public void initTransitionAnimation(Data infoStory, AllStoriesAdapter.ViewHolder view, int position) {


        FragmentNavigator.Extras extras = new FragmentNavigator.Extras.Builder()
                .addSharedElement(view.img_book, "kittenImage")
                .addSharedElement(view.btn_voice, "voiceImg")
                .addSharedElement(view.btn_text, "readImg")
                .addSharedElement(view.bookTitle ,"titleText")
                .addSharedElement(view.input_illustrator ,"inDesigner")
                .addSharedElement(view.input_writer ,"inWriter")
                .build();


        Bundle dataBundle = new Bundle();
        dataBundle.putInt("id" , infoStory.getId());
        dataBundle.putString("designer" ,infoStory.getDesigner());
        dataBundle.putString("storyName" ,infoStory.getName());
        dataBundle.putString("Talker" ,infoStory.getTalker());
        dataBundle.putString("storyTitle" ,infoStory.getTitle());
        dataBundle.putString("writer" ,infoStory.getWriter());
        dataBundle.putString("img" ,infoStory.getImageBookLink());
        dataBundle.putString("voice_link" , infoStory.getVoiceLink());
        dataBundle.putBoolean("isLiked" , infoStory.getIs_like());

        NavOptions navOptions = new NavOptions.Builder()
                .setLaunchSingleTop(true)  // Used to prevent multiple copies of the same destination
                .setEnterAnim(R.anim.nav_default_enter_anim)
                .setExitAnim(R.anim.nav_default_exit_anim)
                .setPopEnterAnim(R.anim.nav_default_pop_enter_anim)
                .setPopExitAnim(R.anim.nav_default_pop_exit_anim)

    .build();

        navController.navigate(R.id.detailsStoryFragment , dataBundle ,navOp  ,extras);

    }
}
