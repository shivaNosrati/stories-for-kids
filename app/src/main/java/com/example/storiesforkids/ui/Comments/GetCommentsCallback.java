package com.example.storiesforkids.ui.Comments;

public interface GetCommentsCallback {

    void onReceive(CommentModel commentModel);
    void notReceive(Throwable t);
}
