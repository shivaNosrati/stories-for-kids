package com.example.storiesforkids.ui.MainView;

import com.example.storiesforkids.ui.BaseFragment;

public interface SomeChangeInView {

    void onFragmentChangeListener(BaseFragment fragment);

    void visibleUi();
}
