package com.example.storiesforkids.ui.History;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;
import com.bumptech.glide.Glide;
import com.example.storiesforkids.R;
import com.example.storiesforkids.ui.Favorite.Data;
import com.example.storiesforkids.ui.Favorite.FavoriteFragment;
import com.example.storiesforkids.ui.MainView.MainActivity;
import java.util.List;

public class HistoryAdapter extends RecyclerView.Adapter<HistoryAdapter.HistoryViewHolder>{

    private List<HistoryData> dataList;
    private Context context;
    private ClickOnReadImg clickItems;


    public HistoryAdapter(List<HistoryData> dataList , Context context , MainActivity mainActivity) {

        this.dataList=dataList;
        this.context =context;
        this.clickItems = mainActivity;

    }

    @NonNull
    @Override
    public HistoryAdapter.HistoryViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View v = LayoutInflater.from(parent.getContext()).inflate(R.layout.history_item, parent, false);
        HistoryAdapter.HistoryViewHolder vh = new HistoryAdapter.HistoryViewHolder(v);
        return vh;
    }

    @Override
    public void onBindViewHolder(@NonNull HistoryViewHolder holder, int position) {

        String img = dataList.get(position).getImageBookLink();
        String newString = img.replace("https", "http");

        if (dataList.get(position).getImageBookLink() == null) {
            holder.img_blank.setVisibility(View.VISIBLE);


        }else {
            Glide.with(context).load(newString).into(holder.img_book);
            holder.img_book.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {

                    clickItems.goToDetails(dataList.get(position));

                }
            });
        }

    }


    @Override
    public int getItemCount() {
        return dataList.size();
    }


    public class HistoryViewHolder extends RecyclerView.ViewHolder {

        private ImageView img_book;
        private ImageView img_blank;


        public HistoryViewHolder(View v) {
            super(v);

            img_book =  v.findViewById(R.id.img_his_item);
            img_blank =  v.findViewById(R.id.img_his_blank);

        }
    }
}
