package com.example.storiesforkids.ui.Favorite;

import android.app.ProgressDialog;
import android.content.Context;
import android.content.pm.ActivityInfo;
import android.os.Build;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.navigation.NavController;
import androidx.navigation.Navigation;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.example.storiesforkids.R;
import com.example.storiesforkids.ui.History.GetHistoryCallback;
import com.example.storiesforkids.ui.History.HistoryAdapter;
import com.example.storiesforkids.ui.History.HistoryData;
import com.example.storiesforkids.ui.History.HistoryModel;
import com.example.storiesforkids.ui.MainView.MainActivity;
import com.example.storiesforkids.ui.Service.NetworkService;
import com.example.storiesforkids.ui.Share.SharePreferencesHelper;

import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;


public class FavoriteFragment extends Fragment {

    private NavController navController;

    @BindView(R.id.recyclerView_like_fave)
    RecyclerView recyclerView_fave;

    private FaveAdapter adapter;
    private HistoryAdapter hAdapter;

    @BindView(R.id.recyclerView_read_fave)
    RecyclerView recyclerView_history;

    @BindView(R.id.img_blank)
    ImageView imgBlank;

    @BindView(R.id.img_blank_history)
    ImageView imgBlank_history;

    private ImageView progressBar;
    Animation animation;
    private FrameLayout progressBarHolder;
    private ProgressDialog progressDialog;
    private String token;


    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.favorite_fragment , container , false);

        ((MainActivity)getActivity()).updateStatusBarColor("#FF4F8F");

        ButterKnife.bind(this , view);

        navController = Navigation.findNavController(getActivity(), R.id.nav_host_fragment);

        token= SharePreferencesHelper.getInatance().getStringValueFromShare(getContext());


        MainActivity activity = (MainActivity) getActivity();
        activity.onGetView(this);


        return view;
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        initProgressFave(view);

    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);

        sendRequestToGetFavorite(token);
        senRequestToGetHistory(token);
    }

    private void senRequestToGetHistory(String token) {

        NetworkService.getService().getHis(token, new GetHistoryCallback() {
            @Override
            public void historyIsSet(HistoryModel historyModel) {

                fillHistoryList(historyModel.getData());
            }

            @Override
            public void historyNotSet(Throwable t) {
//                Toast.makeText(getContext(), ""+t.getMessage(), Toast.LENGTH_SHORT).show();
            }
        });

    }

    private void fillHistoryList(List<HistoryData> hList) {

        if (hList.size() == 0){
            imgBlank_history.setVisibility(View.VISIBLE);
        }else {
            imgBlank_history.setVisibility(View.GONE);
            hAdapter = new HistoryAdapter(hList , getContext() , (MainActivity) getActivity());
            LinearLayoutManager layoutManager = new LinearLayoutManager(getContext(), LinearLayoutManager.HORIZONTAL, false);
            recyclerView_history.setLayoutManager(layoutManager);
            recyclerView_history.setAdapter(
                    hAdapter);
        }

    }

    private void sendRequestToGetFavorite(String token) {

        NetworkService.getService().getFav(token, new GetFaveCallBack() {
            @Override
            public void faveListReceived(FavoritModel data) {
               hideProgressFave();
                fillFavoriteList(data.getData());

            }

            @Override
            public void faveListNotReceived(Throwable t) {
                hideProgressFave();
                                                                                                              
            }
        });
    }

    private void fillFavoriteList(List<FaveData> data) {
        if (data.size() ==0)
            imgBlank.setVisibility(View.VISIBLE);
        else {
            imgBlank.setVisibility(View.GONE);
            adapter = new FaveAdapter(data , getContext() , (MainActivity) getActivity());
            LinearLayoutManager layoutManager = new LinearLayoutManager(getContext(), LinearLayoutManager.HORIZONTAL, false);
            recyclerView_fave.setLayoutManager(layoutManager);
            recyclerView_fave.setAdapter(adapter);
        }

    }





    @Override
    public void onPause() {
        super.onPause();

        hideProgressFave();
        if (android.os.Build.VERSION.SDK_INT >= 27) {
            getActivity().setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_PORTRAIT);
        }
    }

    @Override
    public void onResume() {
        super.onResume();
        getActivity().setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_PORTRAIT);
        ((MainActivity) getActivity()).visibleUi();

    }

    private void initProgressFave(View view) {
        progressBar = view.findViewById(R.id.progress);
        progressBarHolder = view.findViewById(R.id.progressBarHolder);
        animation = AnimationUtils.loadAnimation(getContext(), R.anim.clock_anim);
        animation.setRepeatCount(Animation.INFINITE);
        progressBarHolder.setVisibility(View.VISIBLE);
        progressBar.setVisibility(View.VISIBLE);
        progressBar.startAnimation(animation);

    }

    private void hideProgressFave() {

        progressBarHolder.setVisibility(View.GONE);
        progressBar.setVisibility(View.GONE);
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.FROYO) {
            animation.cancel();
        }

    }




}
